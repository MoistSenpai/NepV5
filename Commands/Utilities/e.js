const path = require('path');
const emojiTree = require('emoji-tree');
const emojiUnicode = require('emoji-unicode')

module.exports.run = (client, msg, args) => {
  try {
    let uniEmote = emojiTree(`${args.join(' ')}`);
    let emote = /(?:<(a?):\w{2,32}:(\d{18})>)|(\d{18})/;


    if (!emote.test(args.join(' ')) && isEmote(args.join(' '))) return msg.channel.send({
        embed: new client.discord.MessageEmbed()
        .setDescription(`[Direct Link](https://assets-cdn.github.com/images/icons/emoji/unicode/${emojiUnicode(args.join(' '))}.png?v6)`)
        .setImage(`https://assets-cdn.github.com/images/icons/emoji/unicode/${emojiUnicode(args.join(' '))}.png?v6`)
        .setColor(client.rColor)
      });

    let match = emote.exec(args.join(' '));
    let gotEmote = `https://cdn.discordapp.com/emojis/${!isNaN(match[0]) ? match[0] : match[2]}${match[1] == 'a' ? '.gif' : '.png'}?size=2048&_=${match[1] == 'a' ? '.gif' : '.png'}`;

    // https://xn--i-7iq.ws/emoji-image/%F0%9F%A4%93.png?format=emojione&ar=2x2


    msg.channel.send({embed: new client.discord.MessageEmbed()
      .setDescription(`[Direct Link](https://cdn.discordapp.com/emojis/${match[2]}${match[1] == 'a' ? '.gif' : '.png'})`)
      .setImage(gotEmote)
      .setColor(client.rColor)
    });
  } catch (err) {
    // msg.channel.send(err.stack, {code: 'css'});
    return msg.channel.send({embed: new client.discord.MessageEmbed()
      .setDescription(`:x: | I can't **find an emote** for \`${args.join(' ')}\`!`)
      .setColor(client.rColor)
    });
  }
}

function isEmote(str) {
  let reg = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32-\ude3a]|[\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/;
  if (reg.test(str) == true) return true;
  else return false;
}

module.exports.info = {
  name: path.basename(__filename, '.js'),
  category: path.dirname(__filename).split(path.sep).pop(),
  help: `Get an image of an emoji`,
  longHelp: `Returns a picture of an emoji, from all across Discord.`,
  example: `${path.basename(__filename, '.js')}`,
  locked: false
}
