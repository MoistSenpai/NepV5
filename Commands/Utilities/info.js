const path = require(`path`);
const os = require('os');
const osName = require('os-name');
const moment = require('moment');

module.exports.run = (client, msg, args) => {
    msg.channel.send({
        embed: new client.discord.MessageEmbed()
            .setColor(client.rColor)
            .addField(`🤖 OS/Internals`, `OS: \`${osName()} (${os.arch()})\`\nCPU: \`${os.cpus()[0].model}\`\nNode: \`${process.version}\``)
            .addField(`🎮 Discord Stuff`, `Discord.JS: \`${client.discord.version}\`\nCreated At: \`${moment(client.user.createdTimestamp).format('MM/DD/YYYY')}\`\nJoined Guild: \`${moment(msg.guild.members.get(client.user.id).joinedTimestamp).format('MM/DD/YYYY')}\``)
            .addField(`<:NepDoggo:390652474148651038> Me Stuff`, `Guild Count: \`${client.guilds.size}\`\nOwner(s): \`${client.users.get(`184157133187710977`).tag} and ${client.users.get(`251091302303662080`).tag}\`\nCurrent Prefix: \`${client.prefix}\``)
            .setFooter(client.user.tag, client.user.avatarURL())
    });

}

module.exports.info = {
    name: path.basename(__filename, '.js'),
    category: path.dirname(__filename).split(path.sep).pop(),
    help: `Info about me nobody cares about.`,
    longHelp: `Gives you info about OS, Discord, and Myself.`,
    example: `${path.basename(__filename, '.js')}`,
    locked: false
}