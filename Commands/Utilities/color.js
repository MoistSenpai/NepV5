const path = require(`path`);
const convert = require('color-convert');
const {
    MessageEmbed
} = require('discord.js');

const rgbToHSL = (red, green, blue) => {
    let r = red / 255;
    let g = green / 255;
    let b = blue / 255;

    let max = Math.max(r, g, b),
        min = Math.min(r, g, b);
    let h, s, l = (max + min) / 2;

    if (max == min) {
        h = s = 0; // Achromatic
    } else {
        let d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch (max) {
            case r:
                h = (g - b) / d + (g < b ? 6 : 0);
                break;
            case g:
                h = (b - r) / d + 2;
                break;
            case b:
                h = (r - g) / d + 4;
                break;
        }
        h /= 6;
    }

    h = Math.round(h * 360);
    s = Math.round(s * 100);
    l = Math.round(l * 100);

    return {
        hue: h,
        saturation: s,
        lightness: l
    }
}

const resolveColor = input => {
    if (input.startsWith('#')) input = input.substr(1);
    if (input.length === 3) input = input.split('').map(c => c + c).join('');

    let hex = input;
    let [red, green, blue] = [hex.substr(0, 2), hex.substr(2, 2), hex.substr(4, 2)].map(value => parseInt(value, 16));
    let {
        hue,
        saturation,
        lightness
    } = rgbToHSL(red, green, blue);

    return {
        hex,
        red,
        green,
        blue,
        hue,
        saturation,
        lightness
    };
}

module.exports.run = (client, msg, args) => {
    let colour;

    if (!args[0]) return msg.channel.send({
        embed: new MessageEmbed()
            .setDescription(`:x: | You must provide a colour!`)
            .setColor(client.rColor)
    });

    try {
        if (!/^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/.test(args.join(' '))) c = convert.keyword.hex(args.join(' ').toLowerCase());
        else c = args[0];
    } catch (err) {
        c = args[0];
    }

    if (!/^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/.test(c)) {
        return msg.channel.send({
            embed: new MessageEmbed()
                .setDescription(`:x: | The color must be in the format of \`#RRGGBB\`, \`#RGB\` or \`red, lightblue, etc.\`!`)
                .setColor(client.rColor)
        });
    }

    let color = resolveColor(c);

    return msg.channel.send({
        embed: new MessageEmbed()
            .setTitle(`🎨 ${args.join(' ').toUpperCase()}:`)
            .setDescription(`HEX: \`#${color.hex}\`\nRGB: \`${color.red}, ${color.green}, ${color.blue}\`\nHSL: \`${color.hue}, ${color.saturation}, ${color.lightness}\``)
            .setImage(`http://placehold.it/500/${color.hex}/${color.hex}`)
            .setColor(`${color.hex}`)
    });
}

module.exports.info = {
    name: path.basename(__filename, '.js'),
    category: path.dirname(__filename).split(path.sep).pop(),
    help: `Get a preview of a hex colour.`,
    longHelp: `Returns information and preview for the provided hex colour.`,
    example: `${path.basename(__filename, '.js')} <Hex, Colour Name>`,
    locked: false
}
