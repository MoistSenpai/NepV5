const path = require('path');

module.exports.run = (client, msg, args) => {
    let listArr = [];
    let roleList = [];
    let checkArr = [];
    let findRole = msg.guild.roles.find((role) => role.name.toLowerCase() == args.slice(1).join(' ').toLowerCase());

    client.connection.query(`SELECT * FROM setrole WHERE guildId = ${msg.guild.id}`, function (err, row) {
        let rowArr = [];
        for (let i = 0; i < row.length; i++) {
            rowArr.push(`<@&${row[i]['roleId']}>`);
        }
        let roles = rowArr.map((r) => r).join(' | ');

        if (!args[0] || args == '' || args === undefined || !args[0].toLowerCase().startsWith(`-add`) && !args[0].toLowerCase().startsWith(`-remove`)) {
            let seperatedList = []; // Seperated Arr

            if (row.length <= 0) return msg.channel.send({
                embed: new client.discord.MessageEmbed()
                    .setDescription(`*There's nothing here! Do \`${client.prefix}setrole\` to add some roles!*`)
                    .setColor(client.rColor)
            });

            for (let i = 0; i < row.length; i++) {
                listArr.push(`${msg.guild.roles.get(row[i]['roleId'])}`);
                roleList.push(`<@&${row[i]['roleId']}>`);
            }

            let list = roleList.map((r) => r).join(' | ');

            checkArr.push(list);

            if (list.length >= 1024) {
                while (listArr.length) { // While reactions in arr split and join new line for seperated arr
                    seperatedList.push(listArr.splice(0, 10).join('\n')); // 1-x list
                }

                msg.channel.send({
                    embed: new client.discord.MessageEmbed()
                        .setDescription(`<a:PenguinDance:429390316261605396> -=-= \`${client.prefix}role -add <Role>\` or \`${client.prefix}role -remove <Role>\` =-=- <a:PenguinDance:429390316261605396>\n\n${seperatedList[0]}`)
                        .setColor(client.rColor)
                }).then(async (toReact) => { // Send first page
                    await toReact.react('◀'); // React Left
                    await toReact.react('⚪'); // React center
                    await toReact.react('▶'); // React right

                    // Set up reaction collector
                    const collector = new client.discord.ReactionCollector(toReact, m => m.users.last().id == msg.author.id, {
                        time: 60000
                    });

                    let counter = 0; // Page counter

                    collector.on('collect', (r) => {
                        if (r.emoji.name == '◀' && counter >= 0) {
                            counter-- // Minus page
                            if (counter == -1) counter = 0;
                            toReact.edit({
                                embed: new client.discord.MessageEmbed()
                                    .setDescription(`<a:PenguinDance:429390316261605396> -=-= \`${client.prefix}role -add <Role>\` or \`${client.prefix}role -remove <Role>\` =-=- <a:PenguinDance:429390316261605396>\n\n${seperatedList[counter]}`)
                                    .setColor(client.rColor)
                            }); // Show page
                        }
                        if (r.emoji.name == '▶' && counter < seperatedList.length - 1) {
                            counter++ // Add page
                            toReact.edit({
                                embed: new client.discord.MessageEmbed()
                                    .setDescription(`<a:PenguinDance:429390316261605396> -=-= \`${client.prefix}role -add <Role>\` or \`${client.prefix}role -remove <Role>\` =-=- <a:PenguinDance:429390316261605396>\n\n${seperatedList[counter]}`)
                                    .setColor(client.rColor)
                            }); // Show page
                        }
                    });
                    collector.on('remove', (r) => {
                        if (r.emoji.name == '◀' && counter >= 0) {
                            counter-- // Minus page
                            if (counter == -1) counter = 0;
                            toReact.edit({
                                embed: new client.discord.MessageEmbed()
                                    .setDescription(`<a:PenguinDance:429390316261605396> -=-= \`${client.prefix}role -add <Role>\` or \`${client.prefix}role -remove <Role>\` =-=- <a:PenguinDance:429390316261605396>\n\n${seperatedList[counter]}`)
                                    .setColor(client.rColor)
                            }); // Show page
                        }
                        if (r.emoji.name == '▶' && counter < seperatedList.length - 1) {
                            counter++ // Add page
                            toReact.edit({
                                embed: new client.discord.MessageEmbed()
                                    .setDescription(`<a:PenguinDance:429390316261605396> -=-= \`${client.prefix}role -add <Role>\` or \`${client.prefix}role -remove <Role>\` =-=- <a:PenguinDance:429390316261605396>\n\n${seperatedList[counter]}`)
                                    .setColor(client.rColor)
                            }); // Show page
                        }
                    });
                });
            } else {
                return msg.channel.send({
                    embed: new client.discord.MessageEmbed()
                        .setDescription(`<a:PenguinDance:429390316261605396> -=-= \`${client.prefix}role -add <Role>\` or \`${client.prefix}role -remove <Role>\` =-=- <a:PenguinDance:429390316261605396>\n\n${list}`)
                        .setColor(client.rColor)
                }).catch((e) => client.error(client, msg, e));
            }
        }
        if (!args[1]) return msg.channel.send({
            embed: new client.discord.MessageEmbed()
                .setDescription(`:x: | You need to **supply a role** dummy!`)
                .setColor(client.rColor)
        });
        if (args[0].toLowerCase().startsWith(`-add`)) { // Add Role
            let list = roleList.map((r) => r).join(' | ');

            if (!findRole) return msg.channel.send({
                embed: new client.discord.MessageEmbed()
                    .setDescription(`:x: | I couldn't find a role for \`${args.slice(1).join(' ').toUpperCase()}\`, git gud!`)
                    .setColor(client.rColor)
            });

            if (roles.toLowerCase().indexOf(findRole) <= -1) return msg.channel.send({
                embed: new client.discord.MessageEmbed()
                    .setDescription(`:x: | \`${args.slice(1).join(' ').toUpperCase()}\` isn't on the role list, go away hacker!`)
                    .setColor(client.rColor)
            });

            new Promise((resolve, reject) => {
                msg.member.roles.add(findRole).then((added) => {
                    msg.channel.send({
                        embed: new client.discord.MessageEmbed()
                            .setDescription(`**<:JibrilDrool:390652488912470017> | Added role [${findRole}] to [${msg.author}]!**`)
                            .setColor(client.rColor)
                    });
                    resolve(added);
                }).catch((err) => {
                    return client.error(client, msg, err, true);
                    reject();
                });
            });
        }
        if (args[0].toLowerCase().startsWith(`-remove`)) { // Remove Role
            let list = roleList.map((r) => r).join(' | ');

            if (!findRole) return msg.channel.send({
                embed: new client.discord.MessageEmbed()
                    .setDescription(`:x: | I couldn't find a role for \`${args.slice(1).join(' ').toUpperCase()}\`, git gud!`)
                    .setColor(client.rColor)
            });
            if (roles.indexOf(findRole) <= -1) return msg.channel.send({
                embed: new client.discord.MessageEmbed()
                    .setDescription(`:x: | \`${args.slice(1).join(' ').toUpperCase()}\` isn't on the role list, go away hacker!`)
                    .setColor(client.rColor)
            });

            new Promise((resolve, reject) => {
                msg.member.roles.remove(findRole).then((removed) => {
                    msg.channel.send({
                        embed: new client.discord.MessageEmbed()
                            .setDescription(`**<:Tsumiki:390652488963063820> | Removed role [${findRole}] from [${msg.author}]!**`)
                            .setColor(client.rColor)
                    });
                    resolve(removed);
                }).catch((err) => {
                    return msg.channel.send(`**<:Nyo:390652489369649162> | OwO what's this? An error occured!**\n\`\`\`js\n${err.message}\n\`\`\``);
                    reject();
                });
            });
        }
    });
}

module.exports.info = {
    name: path.basename(__filename, '.js'),
    category: path.dirname(__filename).split(path.sep).pop(),
    help: `Adds or removes a role from preset role list.`,
    longHelp: `Adds or removes role from user from set roles (--setrole).`,
    example: `${path.basename(__filename, '.js')} <-add, or -remove> <Role>`,
    locked: false,
    dm: false
}
