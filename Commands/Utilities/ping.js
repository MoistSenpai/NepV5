const path = require(`path`);

module.exports.run = (client, msg, args) => {
    msg.channel.send(`*Pinging...*`).then((m) => {
        m.edit({
            embed: new client.discord.MessageEmbed()
                .setDescription(`:ping_pong: *Why are you using this?*\n<a:fancy_parrot:435473415613186058> | **Nepu:** \`${Math.round(client.ping)}ms\`\n<a:disdat:423642260916404224> | **Latancy:**  \`${Math.round(m.createdTimestamp - msg.createdTimestamp)}ms\``)
                .setColor(client.rColor)
        });
    });

    // msg.channel.send(`<a:fancy_parrot:435473415613186058>`).then(msgd => msgd.edit(` Round: ${Math.round((msgd.createdTimestamp - msg.createdTimestamp))}ms   \nAPI: ${Math.round(client.ping)}ms `));
}

module.exports.info = {
    name: path.basename(__filename, '.js'),
    category: path.dirname(__filename).split(path.sep).pop(),
    help: `Generic 'ping-pong' command.`,
    longHelp: `Returns the round trip ping and the AP, latency.`,
    example: `${path.basename(__filename, '.js')}`,
    locked: false
}