const path = require('path');
const solve = require('math-expression-evaluator');

module.exports.run = (client, msg, args) => {
    if (!args[0]) return msg.channel.send({
        embed: new client.discord.MessageEmbed()
            .setDescription(`:x: | Give **expression to solve** with my sicc skills!`)
            .setColor(client.rColor)
    });

    try {
        let answer = solve.eval(args.join(' '));

        msg.channel.send({
            embed: new client.discord.MessageEmbed()
                .setDescription(`🔶 **Equation** | \`${args.join(' ')}\`\n\n🔷 **Answer** | \`${answer.toFixed(2)}\``)
                .setColor(client.rColor)
        });

    } catch (err) {
        msg.channel.send({
            embed: new client.discord.MessageEmbed()
                .setDescription(`:x: | Invalid Equation!\n\`\`\`${err.message}\n\`\`\``)
                .setColor(client.rColor)
        });
    }
}

module.exports.info = {
    name: path.basename(__filename, '.js'),
    category: path.dirname(__filename).split(path.sep).pop(),
    help: `Solves math expressions.`,
    longHelp: `Solve a math expression..`,
    example: `${path.basename(__filename, '.js')} <Expression>`,
    locked: false
}