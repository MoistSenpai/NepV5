const path = require('path');
const moment = require('moment');

module.exports.run = (client, msg, args) => {
   if (!args[0]) return sendInfo(msg.member);

  client.listUsers(client, msg, args.join(' '), function(err, user) {
    if (err) return err;
    else return sendInfo(user);
  });

  function sendInfo(dude) {
    dude = msg.guild.members.get(dude.id);

    if (dude.presence.activity == null || dude.presence.activity.type !== 'PLAYING') gameMessage = 'Nothing';
    else gameMessage = dude.presence.activity.name;

    msg.channel.send({
      embed: new client.discord.MessageEmbed()
        .setDescription(`**| <a:MikuDance:422159573344845844> \`${dude.bot ? dude.tag : dude.user.tag} ${dude.user.bot ? '< Bot >' : '< Human >'}\` <a:MikuDance:422159573344845844> |**`)
        .setThumbnail(dude.bot ? dude.avatarURL : dude.user.avatarURL)
        .addField(`📋 Nickname`, `**${dude.nickname ? dude.nickname : 'None'}**`, true)
        .addField(`🔮 Status`, `**${dude.presence.status.toUpperCase()}**`, true)
        .addField(`🆔 ID`, `**${dude.id}**`, true)
        .addField(`🎮 Game Playing`, `**${gameMessage}**`, true)
        .addField(`🕰 Joined Guild`, `**${moment(dude.joinedAt).format('MMMM Do YYYY')}**`, true)
        .addField(`🗓 Joined Discord`, `**${moment(dude.user.createdAt).format('MMMM Do YYYY')}**`, true)
        .setColor(dude.displayColor)
        .setThumbnail(dude.user.avatarURL())
    });
  }
}

module.exports.info = {
  name: path.basename(__filename, '.js'),
  category: path.dirname(__filename).split(path.sep).pop(),
  help: `Gives you info about a user.`,
  longHelp: `Returns info about yourself or a user.`,
  example: `${path.basename(__filename, '.js')} [Mention, ID, Tag, or Username]`,
  locked: false
}
