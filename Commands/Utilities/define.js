const path = require(`path`);
const curl = require('curl');

module.exports.run = (client, msg, args) => {
  if (!args[0]) return msg.channel.send({
    embed: new client.discord.MessageEmbed()
    .setDescription(`:x: | Woh dud, you **need the correct args**! Do \`${client.prefix }help ${path.basename(__filename, '.js')}\` for more info!`)
    .setColor(client.rColor)
  });

try {

  curl.getJSON(`http://nepnep.tk/api/dictionary?word=${args.join(' ')}`, (err, resp, bod) => {
    if (err) return msg.channel.send(err, {code: 'css'});
    else if (bod.status == 400) return msg.channel.send({
     embed: new client.discord.MessageEmbed()
       .setDescription(`:x: | No definition on my API found for \`${args.join(' ').length >= 1024 ? args.join(' ').substr(Math.floor(args.join(' ').length / 2), args.join(' ').length) + '...' : args.join(' ')}\`, get good!`)
       .setColor(client.rColor)
   });

   let deffArr = [];

   for (let i = 0; i < bod.list.definition.length; i++) {

     if (typeof bod.list.definition[i] == 'object') { continue; }
     else if (bod.list.definition[i].length <= 1) { continue; }

     deffArr.push(`• *${bod.list.definition[i].replace(bod.list.definition[i][0], '').replace(bod.list.definition[i][1], bod.list.definition[i][1].toUpperCase())}*`);
   }

   if (!deffArr || deffArr == undefined || deffArr == '') return msg.channel.send({
    embed: new client.discord.MessageEmbed()
      .setDescription(`:x: | No definition on my API found for \`${args.join(' ').length >= 1024 ? args.join(' ').substr(Math.floor(args.join(' ').length / 2), args.join(' ').length) + '...' : args.join(' ')}\`, get good!`)
      .setColor(client.rColor)
  });

   msg.channel.send({
     embed: new client.discord.MessageEmbed()
      .setDescription(`📚 | **Definition for** \`${bod.word.replace(bod.word[0], bod.word[0].toUpperCase())}\`\n\n${deffArr.join('\n\n')}`)
      .setColor(client.rColor)
   });

  });

  } catch(err) {
     msg.channel.send({
      embed: new client.discord.MessageEmbed()
        .setDescription(`:x: | No definition on my API found for \`${args.join(' ').length >= 1024 ? args.join(' ').substr(Math.floor(args.join(' ').length / 2), args.join(' ').length) + '...' : args.join(' ')}\`, get good!`)
        .setColor(client.rColor)
    });
  }

}
// .setDescription(`:x: | No definition found for \`${args.join(' ').length >= 1024 ? args.join(' ').substr(Math.floor(args.join(' ').length / 2), args.join(' ').length) + '...' : args.join(' ')}\`, get good!`)

module.exports.info = {
  name: path.basename(__filename, '.js'),
  category: path.dirname(__filename).split(path.sep).pop(),
  help: `Generic dictionary command.`,
  longHelp: `Gives definitions of words from dictionary.com.`,
  example: `${path.basename(__filename, '.js')} <Word>`,
  locked: false
}
