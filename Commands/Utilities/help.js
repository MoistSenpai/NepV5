const path = require(`path`);
const fs = require(`fs`);
const dir = `/home/moist/Bots/NepV4`;

module.exports.run = (client, msg, args) => {
  let categoryList = fs.readdirSync(`${dir}/Commands/`);

  let commandArr = [];
  let categoryArr = [];
  let categoryArr2 = [];

  // Push all commands & categories into arr

  for (let i = 0; i < categoryList.length; i++) {
    let commandList = fs.readdirSync(`${dir}/Commands/${categoryList[i]}/`);

    categoryArr.push(categoryList[i]);
    categoryArr2.push(`• **${categoryList[i]}**`);
    for (let j = 0; j < commandList.length; j++) {
      let prop = commandList[j];
      if (prop.match(/\.js$/)) commandArr.push(prop.replace(/\.[^/.]+$/, ''));
    }
  }

  if (!args[0]) return msg.channel.send({
    embed: new client.discord.MessageEmbed()
    .setDescription(`| \`${client.prefix}${path.basename(__filename, '.js')} <command>\` *or* \`${client.prefix}${path.basename(__filename, '.js')} <category>\` |`)
    .addField(`Categories`, `${categoryArr2.join('\n')}`)
    .addField(`Servers`, `*• [Support Server](https://discord.gg/R9ykDC3)*`)
    .setColor(client.rColor)
  });


  if (args[0].toLowerCase() == 'all') { // Send all help
    for (let i = 0; i < categoryArr.length; i++) {
      sendCategoryHelp(msg.author, categoryArr[i]);
    }
    return msg.react(`423642260916404224`);
  }

  // Handle which category to send

  commandArr.forEach((command) => {
    if (args[0].toLowerCase().startsWith(command)) return sendLong(command);
  });
  categoryArr.forEach((category) => {
    if (args[0].toLowerCase().startsWith(category.toString().toLowerCase().substr(0, 3))) return sendCategoryHelp(msg.channel, category);
  });


  function sendCategoryHelp(channel, category) {
    // Send all commands in category

    let commandInfoArr = [];
    let seperatedList = [];

    for (let command in client.commands) {
      if (client.commands[command].info.category == category) {
        commandInfoArr.push(`**${client.prefix}${client.commands[command].info.name}** - *${client.commands[command].info.help}*`);
      }
    }

    // Handle pages if it's too long and that kind of stuff

    if (commandInfoArr.length >= 1024) {
      while(commandInfoArr.length) {
        seperatedList.push( commandInfoArr.splice(0, 10).join('\n') );
      }

      if (channel == msg.author) {
        for (let i = 0; i < seperatedList.length; i++) {
          channel.send({
            embed: new client.discord.MessageEmbed()
            .setDescription(`<a:disdat:423642260916404224> **=-=-=** __${category}__ **-=-=-=** <a:disdat:423642260916404224>\n\n${seperatedList[i]}`)
            .setColor(client.rColor)
          });
        }
      }

      channel.send({
        embed: new client.discord.MessageEmbed()
        .setDescription(`<a:disdat:423642260916404224> **=-=-=** __${category}__ **-=-=-=** <a:disdat:423642260916404224>\n\n${seperatedList[0]}`)
        .setColor(client.rColor)
      }).then((toReact) => {
        toReact.react(`◀`).then(() => toReact.react(`▶`));

        let collector = toReact.createReactionCollector((r) => r.users.last().id == msg.author.id, {time: 60000});
        let counter = 0;

        collector.on('collect', (r) => {
          if (r.emoji.name == '◀' && counter >= 0) {
              counter--;
              if (counter == -1) counter = 0;
              toReact.edit({
                embed: new client.discord.MessageEmbed()
                .setDescription(`<a:disdat:423642260916404224> **=-=-=** __${category}__ **-=-=-=** <a:disdat:423642260916404224>\n\n${seperatedList[counter]}`)
                .setColor(client.rColor)
              });
            }
            if (r.emoji.name == '▶' && counter < seperatedList.length -1) {
              counter++;
              toReact.edit({
                embed: new client.discord.MessageEmbed()
                .setDescription(`<a:disdat:423642260916404224> **=-=-=** __${category}__ **-=-=-=** <a:disdat:423642260916404224>\n\n${seperatedList[counter]}`)
                .setColor(client.rColor)
              });
            }
        });
        collector.on('remove', (r) => {
          if (r.emoji.name == '◀' && counter >= 0) {
              counter--;
              if (counter == -1) counter = 0;
              toReact.edit({embed: new client.discord.MessageEmbed()
                .setDescription(seperatedList[counter])
                .setColor(client.hex())
              });
            }
            if (r.emoji.name == '▶' && counter < seperatedList.length -1) {
              counter++;
              toReact.edit({embed: new client.discord.MessageEmbed()
                .setDescription(seperatedList[counter])
                .setColor(client.rColor)
              });
            }
        });
      });
    }
    else return channel.send({
      embed: new client.discord.MessageEmbed()
      .setDescription(`<a:disdat:423642260916404224> =-=-= __${category}__ -=-=-= <a:disdat:423642260916404224>\n\n${commandInfoArr.join('\n')}`)
      .setColor(client.rColor)
    });
  }
  function sendLong(command) {
    // Send the command help and example

    msg.channel.send({
      embed: new client.discord.MessageEmbed()
      .addField(`Usage and Example`, `**${client.prefix}${command}** | *${client.commands[command].info.longHelp}*\`\`\`css\nExample: ${client.prefix}${client.commands[command].info.example}\`\`\``)
      .setFooter(`${client.commands[command].info.locked ? 'Locked' : 'Unlocked'}`)
      .setColor(client.rColor)
    });
  }

}

module.exports.info = {
  name: path.basename(__filename, '.js'),
  category: path.dirname(__filename).split(path.sep).pop(),
  help: `List commands.`,
  longHelp: `List commands, examples, and functions.`,
  example: `${path.basename(__filename, '.js')} [args]`,
  locked: false
}
