const path = require(`path`);
const os = require(`os`);
const moment = require(`moment`);

module.exports.run = (client, msg, args) => {
  msg.delete({timeout: 100})
    
  try {
    var embed = new client.discord.MessageEmbed()
    var toEdit = msg.channel.send({embed: new client.discord.MessageEmbed()
      .addField('What info would you like to see out of this list:',
        `\`bot\`: I'll give you a business card.` +'\n'+
        `\`guild\`: I'll give you a key to my house.` +'\n'+
        '[30s]'
      ).setColor(client.rColor)
    })
  .then((toEdit) => {
    msg.channel.awaitMessages(response =>
     response.content.toLowerCase() === 'bot' ||
     response.content.toLowerCase() === 'guild', {
      max: 1,
      time: 30000,
      errors: ['time'],
    })
    .then((collected) => {
      collected.forEach(msgy => {msgy.delete({timeout: 1000})});
        if (collected.first().content.toLowerCase() == 'bot') {
         client.generateInvite().then(link => {
            embed.addField(`Want me to work for you?`, `[Here is my business card.](${link})`);
           embed.setFooter(client.user.tag, client.user.displayAvatarURL())
          });
        }
        else if (collected.first().content.toLowerCase() == 'guild') {
          if (!msg.guild.members.get(`${client.user.id}`).hasPermission("CREATE_INSTANT_INVITE")) return msg.channel.send("Sorry! I cannot do that currently. Please request one from the administrators.")
             msg.channel.createInvite({ maxAge: 3600, maxUses: 2, unique: true, reason: `Best server ever with ${msg.guild.owner.user.tag} as the owner!`})
              .then(invite => {
               embed.addField(`Guild invite link`, `Created an invite https://discord.gg/${invite.code}\n`)
               embed.setFooter(msg.guild.name, msg.guild.iconURL())
              }).catch(console.error);
          }
        else {
          toEdit.edit('HALP SOME TING WENT WONG')
        }
      embed.setColor(client.rColor)
        setTimeout(function () {
          toEdit.edit(embed).then(msgd => msgd.delete({timeout: 60000}));
        }, 1000);
      })
      .catch(() => {
        toEdit.edit('There was no collected message that passed the filter within the time limit!');
      });
  })
}
  catch (err) {
    console.log(err);
  }
}

module.exports.info = {
  name: path.basename(__filename, '.js'),
  category: path.dirname(__filename).split(path.sep).pop(),
  help: `Bot invite or Guild invite links.`,
  longHelp: `Choose which type of invite you would like to get: either the bot invitation link to join a guild, or a guild invite to share with your friends.`,
  example: `${path.basename(__filename, '.js')} <type>`,
  locked: false
}