const path = require(`path`);

module.exports.run = (client, msg, args) => {

  client.connection.query(`SELECT * FROM servers WHERE guildId = ${msg.guild.id}`, function(err, row) {
    if (err) return msg.channel.send(err, {code: 'css'});
    msg.channel.send({
      embed: new client.discord.MessageEmbed()
      .setDescription(`🔐 | **My prefix is**: \`${row[0].prefix}\`\n\n**[** ${client.user} prefix **]** (If you ever forget)`)
      .setColor(client.rColor)
    });
  });

}

module.exports.info = {
  name: path.basename(__filename, '.js'),
  category: path.dirname(__filename).split(path.sep).pop(),
  help: `Remind me your prefix.`,
  longHelp: `Return the prefix for me on the current guild.`,
  example: `${path.basename(__filename, '.js')}`,
  locked: false,
  dm: false
}
