const path = require(`path`);
const moment = require('moment');

module.exports.run = (client, msg, args) => {
  if (!args[0]) return guildInfo(msg.guild);

  client.listGuilds(client, msg, args.join(' '), function(err, guild) {
    if (err) return err;
    return guildInfo(guild);
  });

  function guildInfo(guild) {
    let guildName = guild.name.length >= 50 ? guild.name.substr(Math.floor(guild.name.length / 2), guild.name.length) + '...' : guild.name
    let botArr = [];

    guild.members.map((m) => m.user.bot ? botArr.push(m.user.tag) : '');

    // TODO: delt this
    return msg.channel.send({
      embed: new client.discord.MessageEmbed()
      .setColor(client.rColor)
      .setThumbnail(guild.iconURL({size: 2048}))
      .setDescription(`**|** <a:PenguinDance:429390316261605396> \`${guildName}\` <a:PenguinDance:429390316261605396> **|**`)
      .addField(`👥 Members:`, `**${guild.members.size} • ${botArr.length++} Bots**`, true)
      .addField(`👮 Owner:`, `**${guild.owner.user.tag}**`, true)
      .addField(`📜 Roles (${guild.roles.size}):`, `${guild.roles.size > 40 ? '**Too many! (40+)**' : guild.roles.map((r) => guild.id == msg.guild.id ? r : `\`${r.name}\``).join('|')}`, true)
      .addField(`🕙 Created At:`, `**${moment(guild.createdAt).format('MMMM Do YYYY')}**`, true)
      .addField(`🗨 Text Channels:`, `**${guild.channels.filter((c) => c.type == 'text').size}**`, true)
      .addField(`🗣 Voice Channels:`, `**${guild.channels.filter((c) => c.type == 'voice').size}**`, true)
      .addField(`🎩 Categories:`, `**${guild.channels.filter((c) => c.type == 'category').size}**`, true)
      .setFooter(msg.author.tag, msg.author.avatarURL())
    });
  }
}

module.exports.info = {
  name: path.basename(__filename, '.js'),
  category: path.dirname(__filename).split(path.sep).pop(),
  help: `Gives you info on a server and stuff.`,
  longHelp: `Returns info about the server or a server Nep is on.`,
  example: `${path.basename(__filename, '.js')} [Guild ID or Name]`,
  locked: false,
  dm: false
}
