const path = require(`path`);
const curl = require('curl');

module.exports.run = (client, msg, args) => {
  if (!args[0]) return msg.channel.send({
    embed: new client.discord.MessageEmbed()
    .setDescription(`:x: | Woh dud, you **need the correct args**! Do \`${client.prefix }help ${path.basename(__filename, '.js')}\` for more info!`)
    .setColor(client.rColor)
  });

  curl.getJSON(`http://api.urbandictionary.com/v0/define?term=${args.join(' ')}`, (err, resp, bod) => {
    if (err) return msg.channel.send({
      embed: new client.discord.MessageEmbed()
      .setDescription(`:x: | Nice **error** you got there!\`\`\`css\n${err}\`\`\``)
      .setColor(client.rColor)
    });

    try {
      if (bod.result_type == 'no_results') return msg.channel.send({
        embed: new client.discord.MessageEmbed()
        .setDescription(`:x: | No definition found for \`${args.join(' ').length >= 1024 ? args.join(' ').substr(Math.floor(args.join(' ').length / 2), args.join(' ').length) + '...' : args.join(' ')}\`, get good!`)
        .setColor(client.rColor)
      });

        msg.channel.send({embed: new client.discord.MessageEmbed()
          .setDescription(`:books: | \`${args.join(' ')}\`: \n${bod.list[0].definition}\n\n👍 ${bod.list[0].thumbs_up} • 👎 ${bod.list[0].thumbs_down}`)
          .setColor(client.rColor)
        }).then((toReact) => {
          toReact.react('◀').then(() => toReact.react('▶'));

          let collector = new client.discord.ReactionCollector(toReact, m => m.users.last().id == msg.author.id, {time: 60000, dispose: true});
          let counter = 0;

          collector.on('collect', (r) => {
            if (r.emoji.name == '◀' && counter >= 0) {
              counter--;
              if (counter == -1) counter = 0;
              toReact.edit({embed: new client.discord.MessageEmbed()
                .setDescription(`:books: | \`${args.join(' ')}\` (Page ${counter}): \n${bod.list[counter].definition}\n\n👍 ${bod.list[counter].thumbs_up} • 👎 ${bod.list[counter].thumbs_down}`)
                .setColor(client.rColor)
              });
            }
            if (r.emoji.name == '▶' && counter < bod.list.length -1) {
              counter++;
              toReact.edit({embed: new client.discord.MessageEmbed()
                .setDescription(`:books: | \`${args.join(' ')}\` (Page ${counter}): \n${bod.list[counter].definition}\n\n👍 ${bod.list[counter].thumbs_up} • 👎 ${bod.list[counter].thumbs_down}`)
                .setColor(client.rColor)
              });
            }
          });
          collector.on('remove', (r) => {
            if (r.emoji.name == '◀' && counter >= 0) {
              counter--;
              if (counter == -1) counter = 0;
              toReact.edit({embed: new client.discord.MessageEmbed()
                .setDescription(`:books: | \`${args.join(' ')}\` (Page ${counter}): \n${bod.list[counter].definition}\n\n👍 ${bod.list[counter].thumbs_up} • 👎 ${bod.list[counter].thumbs_down}`)
                .setColor(client.rColor)
              });
            }
            if (r.emoji.name == '▶' && counter < bod.list.length -1) {
              counter++;
              toReact.edit({embed: new client.discord.MessageEmbed()
                .setDescription(`:books: | \`${args.join(' ')}\` (Page ${counter}): \n${bod.list[counter].definition}\n\n👍 ${bod.list[counter].thumbs_up} • 👎 ${bod.list[counter].thumbs_down}`)
                .setColor(client.rColor)
              });
            }
          });
        });
      }
      catch (err) {
        return msg.channel.send({
          embed: new client.discord.MessageEmbed()
          .setDescription(`:x: | No definition found for \`${args.join(' ').length >= 1024 ? args.join(' ').substr(Math.floor(args.join(' ').length / 2), args.join(' ').length) + '...' : args.join(' ')}\`, get good!`)
          .setColor(client.rColor)
        });
      }
  });

}

module.exports.info = {
  name: path.basename(__filename, '.js'),
  category: path.dirname(__filename).split(path.sep).pop(),
  help: `Get urban dictionary definitions for words.`,
  longHelp: `Gives you definitions to words according to UrbanDictionary.`,
  example: `${path.basename(__filename, '.js')} <Word>`,
  locked: false
}
