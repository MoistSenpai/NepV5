const path = require('path');
const translate = require('google-translate-api');

module.exports.run = (client, msg, args) => {
    if (!args[0]) return msg.channel.send({
        embed: new client.discord.MessageEmbed()
            .setDescription(`:x: | See *[this](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)* (\`ISO 639-1\`) for language. ${client.prefix }help ${path.basename(__filename, '.js')}\` for more info!`)
            .setColor(client.rColor)
    })
    else if (!args.slice(1)[0]) return msg.channel.send({
        embed: new client.discord.MessageEmbed()
            .setDescription(`:x: | Provide **something** to translate!`)
            .setColor(client.rColor)
    });

    translate(args.slice(1).join(' '), {
        to: args[0]
    }).then((res) => {
        msg.channel.send({
            embed: new client.discord.MessageEmbed()
                .setDescription(`📖 | \`${args.slice(1).join(' ')}\`\n\`\`\`fix\n${res.text}\n\`\`\``)
                .setColor(client.rColor)
        });
    }).catch((err) => {
        client.error(client, msg, err, true);
    });
}

module.exports.info = {
    name: path.basename(__filename, '.js'),
    category: path.dirname(__filename).split(path.sep).pop(),
    help: `Nep translator.`,
    longHelp: `Translate text from or to a language.`,
    example: `${path.basename(__filename, '.js')} <Lang> <Message>`,
    locked: false
}