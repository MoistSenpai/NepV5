const got = require('got');
const cheerio = require('cheerio');
const {
    MessageEmbed
} = require('discord.js');
const path = require('path');

module.exports.run = async (client, msg, args) => {
    if (args.length < 1) return msg.channel.send({
        embed: new MessageEmbed()
            .setDescription(`:x: | Provide a username of a player!`)
            .setColor(client.rColor)
    });

    const username = args[0];

    const uuid = await getUUID(username);
    if (!uuid) return msg.channel.send({
        embed: new MessageEmbed()
            .setDescription(`:x: | That player **could not be found**, did you even try?`)
            .setColor(client.rColor)
    });
    return msg.channel.send({
        embed: new MessageEmbed()
            .setDescription(`=-=-=-= \`${username}\` =-=-=-=`)
            .addField(`📚 Information`, `UUID: **${uuid}**\nSkin: [Download](https://crafatar.com/skins/${uuid}.png)`)
            .setThumbnail(`https://crafatar.com/avatars/${uuid}.png?size=250&overlay=true`)
            .setImage(`https://crafatar.com/renders/body/${uuid}.png?overlay=true?size=250`)
            .setColor(client.rColor)
    });
}

async function getUUID(username) {
    const res = await got(`https://mcuuid.net/?q=${username}`);
    const $ = cheerio.load(res.body);
    const input = $('input')[1];

    if (!input) return;
    return input.attribs['value'];
}

module.exports.info = {
    name: path.basename(__filename, '.js'),
    category: path.dirname(__filename).split(path.sep).pop(),
    help: `Minecraft Player Info.`,
    longHelp: `Returns info about the Minecraft player provided.`,
    example: `${path.basename(__filename, '.js')} <Minecraft Username>`,
    locked: false
}