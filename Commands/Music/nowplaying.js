const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Nowplaying extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Shows you what's currently playing.`,
      longHelp: `Returns info on what's currently playing.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['np'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    let queue = util.getQueue(msg.guild.id);
    let voiceConnection = msg.guild.voiceConnection;

    if (!queue[0]) return util.embed(`:x: | The **queue is empty**, there's nothing is show!`);
    else if (!voiceConnection) return util.embed(`:x: | There isn't even **anything playing**, feels bad.`);

    return msg.channel.send({
      embed: new nep.discord.MessageEmbed()
        .setDescription(`🎶 | **Currently playing** [${queue[0].videos.info.title[0]}](${queue[0].videos.info.url[0]}) **[${queue[0].videos.info.author}]**`)
        .setColor(nep.rColor)
        .setThumbnail(queue[0].videos.thumbnail.medium.url[0])
    });

  }

}

module.exports = Nowplaying;
