const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Queue extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Central hub for music commands.`,
      longHelp: `Modify the queue like add and stuff.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <-add> <Link or Term>`, `• ${nep.prefix}${path.basename(__filename, '.js')} <-list or -sq>`, `• ${nep.prefix}${path.basename(__filename, '.js')} <-clear or -cq>`, `• ${nep.prefix}${path.basename(__filename, '.js')} <-remove> <Item Position>`, `• ${nep.prefix}${path.basename(__filename, '.js')} <-shuffle>`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} -add PomfPomf`, `• ${nep.prefix}${path.basename(__filename, '.js')} https://www.youtube.com/watch?v=fBf2v4mLM8k`, `• ${nep.prefix}${path.basename(__filename, '.js')} -list`, `• ${nep.prefix}${path.basename(__filename, '.js')} -sq`, `• ${nep.prefix}${path.basename(__filename, '.js')} -clear`, `• ${nep.prefix}${path.basename(__filename, '.js')} -cq`, `• ${nep.prefix}${path.basename(__filename, '.js')} -remove 3`, `• ${nep.prefix}${path.basename(__filename, '.js')} -shuffle`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 3e3,
      aliases: ['q'],
      locked: false,
      allowDM: false
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    let queue = util.getQueue(msg.guild.id); // Guild's queue 
    let queueClass = new(require(`${nep.dir}/Classes/QueueClass.js`))(msg, util, args, nep); // Class with queue methods
    
    if (!args[0]) return util.embed(`:x: | Dud, those are **Invalid arguments**, try \`${nep.prefix}help ${this.info.name}\` for more info!`);
    
    switch (args[0].toLowerCase()) {
      // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-list':
      case '-sq':
        queueClass.showQueue(queue);
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-add':
        queueClass.add(queue);
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-shuffle':
        queueClass.shuffle(queue);
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-remove':
        queueClass.remove(queue);
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-clear':
        queueClass.clear(queue);
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-cq':
        queueClass.clear(queue);
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=
      default:
        util.embed(`:x: | Dud, those are **Invalid arguments**, try \`${nep.prefix}help ${this.info.name}\` for more info!`);
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=      
    }



  }
}

module.exports = Queue;
