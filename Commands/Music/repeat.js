const path = require('path');

module.exports.run = (client, msg, args) => {
  let queue = client.getQueue(client, msg.guild.id);
  let repeatQueue = client.getRQueue(client, msg.guild.id);

  let voiceConnection = msg.guild.voiceConnection;
  let dispatcher = voiceConnection ? voiceConnection.player.dispatcher : null;

  if (repeatQueue.length === 0) {
    if (!queue[0]) return msg.channel.send({
      embed: new client.discord.MessageEmbed()
        .setDescription(`:x: | The **queue** is kind of **empty**, I can repeat nothing if you want!`)
        .setColor(client.rColor)
    });
    else if (!voiceConnection) return msg.channel.send({
      embed: new client.discord.MessageEmbed()
        .setDescription(`:x: | I need to be in a **voice channel** first!`)
        .setColor(client.rColor)
    });
    repeatQueue.push(queue[0]);
    msg.channel.send({
      embed: new client.discord.MessageEmbed()
        .setDescription(`✅ | Mmk, I will repeat \`${queue[0].title}\`!`)
        .setColor(client.rColor)
    });
  } else { // If a song is already repeating, repeat, stop or cancel
    msg.channel.send({
      embed: new client.discord.MessageEmbed()
        .setDescription(`Woah there, a song is \`already repeating\`! What do:\n\n💖 **Overwrite it**\n💝 **Stop repeating**\n💔 **Cancel** `)
        .setFooter(msg.author.tag, msg.author.avatarURL())
        .setColor(client.rColor)
    }).then(async (toReact) => {
      await toReact.react(`💖`); // Overwrite
      await toReact.react(`💝`); // Stop
      await toReact.react(`💔`); // Cancel

      const collector = new client.discord.ReactionCollector(toReact, m => m.users.last().id == msg.author.id, {
          time: 30000,
          dipose: true
      });

      collector.on('end', () => toReact.delete(10));
      collector.on('collect', (m) => {
        if (m.emoji.name == '💖') {
          overwrite();
          toReact.reactions.removeAll();
          collector.stop();
        } else if (m.emoji.name == '💝') {
          stop();
          toReact.reactions.removeAll();
          collector.stop();
        } else if (m.emoji.name == '💔') {
          toReact.reactions.removeAll();
          collector.stop();
        }
      });

      function overwrite() {
        repeatQueue[0] = queue[0];
        msg.channel.send({
          embed: new client.discord.MessageEmbed()
            .setDescription(`✅ | Ok then, I will now repeat \`${queue[0].title}\``)
            .setColor(client.rColor)
        });
      }
      function stop() {
        msg.channel.send({
          embed: new client.discord.MessageEmbed()
            .setDescription(`⛔ | Kk, I will stop repeating now!`)
            .setColor(client.rColor)
        });
        repeatQueue.splice(0, repeatQueue.length);
        client.runQueue(client, msg,  queue)
      }

    });
  }


}

module.exports.info = {
    name: path.basename(__filename, '.js'),
    category: path.dirname(__filename).split(path.sep).pop(),
    help: `Repeat first item in the queue. Use again for more options.`,
    longHelp: `Repeats the first item in the queue. Use again to stop repeating!`,
    example: `${path.basename(__filename, '.js')}`,
    locked: false
}
