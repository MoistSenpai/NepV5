const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');
const ytdl = require('ytdl-core');

class Play extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Play the queue or quick play.`,
      longHelp: `Plays the current queue or quick queue and play link/term.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`, `• ${nep.prefix}${path.basename(__filename, '.js')} [YouTube Link]`, `• ${nep.prefix}${path.basename(__filename, '.js')} [Term]`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`, `• ${nep.prefix}${path.basename(__filename, '.js')} Darude Sandstorm`, `• ${nep.prefix}${path.basename(__filename, '.js')} youtube.com/watch?v=dQw4w9WgXcQ`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 5e3,
      aliases: [],
      /*['ping2']*/
      locked: false,
      allowDM: false
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    let queue = util.getQueue(msg.guild.id);
    let voiceConnection = msg.guild.voiceConnection;

    if (!args[0] && !queue) return util.embed(`:x: | The **queue is empty**, add something and try again! \`${nep.prefix}help play\``); // If no args and no queue
    else if (!args[0] && queue && !voiceConnection) return util.playQueue(queue); // If no args and items in queue, play queue
    else if (!args[0] && queue && voiceConnection) return util.embed(`:x: | The queue is **already running** on this server, get good!`); // If queue is already running

    if (args[0] && args.join(' ').replace(/[^\x20-\x7E]+/gu, ' ') == ' ') return util.embed(`:x: | Fully unicode titles **do not work**. Try something else or a link!`);
    // Search Nep API for video info
    curl.getJSON(`http://nepnep.tk/api/yt_video?maxResults=1&search=${args.join(' ').replace(/[^\x20-\x7E]+/gu, ' ')}&key=${nep.config.nepAPI.key}`, (err, resp, bod) => {
      try {
        if (err) return util.error(err.message, true); // Handle Error
        else if (queue.length > 0) { // If items in queue, push info
          bod.videos.info.author = msg.author;
          queue.push(bod);
          util.embed(`<:Selfie:390652489919365131> | Queued \`${bod.videos.info.title[0]}\` by **[${bod.videos.info.author}]**`);
          msg.delete({timeout: 5e3}).catch((err) => err);
        } else { // If Items not in queue, push and play
          bod.videos.info.author = msg.author;
          queue.push(bod);
          util.playQueue(queue);
          msg.delete({timeout: 5e3}).catch((err) => err);
        }

      } catch (err) {
        // If no results
        util.embed(`:x: | No **results could be found** for your query of \`${util.parseArgs(args.join(' '))}\``);
      }
    });

  }
}

module.exports = Play;
