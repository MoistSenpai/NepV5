const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
class Neko extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Random Nekos`,
      longHelp: `Returns a random cat grill.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['nekos'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    let mention = msg.mentions.members.first();
   
    nep.wolke.getRandom({
        type: 'neko',
        filetype: 'gif'
      }).then((res) => {
        return msg.channel.send({
          embed: new nep.discord.MessageEmbed()
            .setDescription(`<:Nekogear:390652486546882588> *I swear I'm not a furry*`)
            .setImage(res.url)
            .setColor(nep.rColor)
        })
      }).catch((err) => {
        util.error(err.message, true);
      })
  }
}


module.exports = Neko;