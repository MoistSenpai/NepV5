const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Whitenibba extends Command {
    constructor(nep) {
        super(nep, {
            name: path.basename(__filename, '.js'),
            help: `White nibba.`,
            longHelp: `Returns a picture of a white nibba`,
            usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
            examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
            category: path.dirname(__filename).split(path.sep).pop(),
            cooldown: 1e3,
            aliases: ['whiten', 'wn'],
            locked: false,
            allowDM: true
        });
    }

    // ---------------------------------------------------------------------------

    run(msg, util, args, nep) {
        let nibbas = [
          `https://thumbs.dreamstime.com/z/happy-man-perfect-white-smile-thumb-up-isolated-background-40905445.jpg`,
          `http://gossiponthis.com/wp-content/uploads/2014/09/snoop-dogg-white-guy-todd.jpg`,
        ];

        msg.channel.send({
          embed: nep.discord.MessageEmbed()
          .setImage(nibbas[Math.floor(Math.random() * nibbas.length)])
        });

    }
}

module.exports = Whitenibba;