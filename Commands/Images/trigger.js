const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Trigger extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Trigger an image or someone.`,
      longHelp: `Sends trigger meme thing for images or users.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <User, image link, image attachment>`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} Moist`, `• ${nep.prefix}${path.basename(__filename, '.js')} http://i.imgur.com/e0u37lt.jpg`, `• ${nep.prefix}${path.basename(__filename, '.js')} *Attach image attachment*`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 3e4,
      aliases: [],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    let validUrl = /(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)/g;
    let triggerClass = new(require(`${nep.dir}/Classes/triggerClass.js`))(msg, util, args, nep, this);

    if (msg.attachments.first()) return triggerClass.url(msg.attachments.first().url);
    else if (!args[0]) return triggerClass.user(msg.author);

    switch (validUrl.test(args.join(' '))) {
      case true:
        return triggerClass.url(args.join(' '));
      case false:
        return triggerClass.user();
    }

  }
}

module.exports = Trigger;
