const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
class Cry extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Express your sadness.`,
      longHelp: `Returns a random anime gif of crying and stuff`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['crying'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    nep.wolke.getRandom({
      type: 'cry',
      filetype: 'gif'
    }).then((res) => {
      return msg.channel.send({
        embed: new nep.discord.MessageEmbed()
          .setDescription(`Feels bad man <:SadUmaru:293558891944345611>`)
          .setImage(res.url)
          .setColor(nep.rColor)
      })
    }).catch((err) => {
      util.error(err.message, true);
    })
  }
}

module.exports = Cry;