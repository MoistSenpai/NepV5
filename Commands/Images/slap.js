const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
class Slap extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Slap some female dogs!`,
      longHelp: `Returns a random anime gif of a slap.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['slaps'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

   run(msg, util, args, nep) {
    if (!args[0]) return calledForUser(`No one to slap? **HERE YOU GO** ${msg.author}!`);

    util.listUsers(args.join(' ')).then((user) => {
      if (user == msg.author) return calledForUser(`No one to slap? **HERE YOU GO** ${msg.author}!`);
      else if (user == nep.user) return calledForUser(`I got **slapped by** ${msg.author} ;-;`);
      else return calledForUser(`${user} **got slapped by** ${msg.author} o:`);
    }).catch((err) => util.error(err));

    function calledForUser(prompt) {
      nep.wolke.getRandom({
      type: 'slap',
      filetype: 'gif'
    }).then((res) => {
      return msg.channel.send({
        embed: new nep.discord.MessageEmbed()
          .setDescription(prompt)
          .setImage(res.url)
          .setColor(nep.rColor)
      })
    }).catch((err) => {
      util.error(err.message, true);
    })
    }
  }
}


module.exports = Slap;
