const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Dog extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Posts woof woofers.`,
      longHelp: `Returns pictures of doggos`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['doggo', 'woof'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    msg.channel.send({
      embed: new nep.discord.MessageEmbed()
        .setDescription(`<:NepDoggo:390652474148651038> ***Wuff~!***`)
        .setImage(`https://api.thedogapi.com/v1/images/search?format=src&timestamp=${new Date().valueOf()}`)
        .setColor(nep.rColor)
    });
  }
}

module.exports = Dog;
