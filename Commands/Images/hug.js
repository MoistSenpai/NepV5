const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
class Hug extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Gives some random hugs!`,
      longHelp: `Returns a random anime gif of a hug.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['hugs'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

   run(msg, util, args, nep) {
     if (!args[0]) return calledForUser(`No one to hug? **Here you go** ${msg.author} ;3`);

     util.listUsers(args.join(' ')).then((user) => {
       if (user == msg.author) return calledForUser(`No one to hug? **Here you go** ${msg.author} ;3`);
       else if (user == nep.user) return calledForUser(`I got **hugged by** ${msg.author} uwu`);
       else return calledForUser(`${user} **got hugged by** ${msg.author}~!`);
     }).catch((err) => util.error(err));

    function calledForUser(prompt) {
      nep.wolke.getRandom({
      type: 'hug',
      filetype: 'gif'
    }).then((res) => {
      return msg.channel.send({
        embed: new nep.discord.MessageEmbed()
          .setDescription(prompt)
          .setImage(res.url)
          .setColor(nep.rColor)
      })
    }).catch((err) => {
      util.error(err.message, true);
    })
    }
  }
}


module.exports = Hug;
