const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
class Shrug extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `🤷`,
      longHelp: `Returns a shrugging gif.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['shrugs'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    let mention = msg.mentions.members.first();
   
    nep.wolke.getRandom({
        type: 'shrug',
        filetype: 'gif'
      }).then((res) => {
        return msg.channel.send({
          embed: new nep.discord.MessageEmbed()
            .setDescription(`🤷`)
            .setImage(res.url)
            .setColor(nep.rColor)
        })
      }).catch((err) => {
        util.error(err.message, true);
      })
  }
}


module.exports = Shrug;