const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
class Smug extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `~Smug~`,
      longHelp: `Returns a smug face gif.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['smugs'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    let mention = msg.mentions.members.first();
   
    nep.wolke.getRandom({
        type: 'smug',
        filetype: 'gif'
      }).then((res) => {
        return msg.channel.send({
          embed: new nep.discord.MessageEmbed()
            .setDescription(`<:TopNep:293560554918772737>`)
            .setImage(res.url)
            .setColor(nep.rColor)
        })
      }).catch((err) => {
        util.error(err.message, true);
      })
  }
}


module.exports = Smug;