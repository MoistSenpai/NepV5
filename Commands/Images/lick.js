const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
class Lick extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Slurp~!`,
      longHelp: `Returns a random anime gif of licking.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['licks'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return calledForUser(`Sluuurp ${msg.author}!`);

    util.listUsers(args.join(' ')).then((user) => {
      if (user == msg.author) return calledForUser(`Sluuurp ${msg.author}!`);
      else if (user == nep.user) return calledForUser(`${msg.author} **licked me** ewww...`);
      else return calledForUser(`${user} **got licked by** ${msg.author}?`);
    }).catch((err) => util.error(err));

    function calledForUser(prompt) {
      nep.wolke.getRandom({
        type: 'lick',
        filetype: 'gif'
      }).then((res) => {
        return msg.channel.send({
          embed: new nep.discord.MessageEmbed()
            .setDescription(prompt)
            .setImage(res.url)
            .setColor(nep.rColor)
        })
      }).catch((err) => {
        util.error(err.message, true);
      })
    }
  }
}


module.exports = Lick;
