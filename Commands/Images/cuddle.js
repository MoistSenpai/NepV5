const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
class Cuddle extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Gives some random cuddles!.`,
      longHelp: `Returns a random anime gif of a cuddle.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['cuddles'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return calledForUser(`Got nobody? **Here you go** ${msg.author} ;3`);

    util.listUsers(args.join(' ')).then((user) => {
      if (user == msg.author) return calledForUser(`Got nobody? **Here you go** ${msg.author} ;3`);
      else if (user == nep.user) return calledForUser(`I got **cuddled by** ${msg.author} ugu!`);
      else return calledForUser(`${user} **got cuddled by** ${msg.author}~!`)
    }).catch((err) => util.error(err));

    function calledForUser(prompt) {
      nep.wolke.getRandom({
      type: 'cuddle',
      filetype: 'gif'
    }).then((res) => {
      return msg.channel.send({
        embed: new nep.discord.MessageEmbed()
          .setDescription(prompt)
          .setImage(res.url)
          .setColor(nep.rColor)
      })
    }).catch((err) => {
      util.error(err.message, true);
    });
    }
  }
}

module.exports = Cuddle;
