const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
class Pout extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Hmph!`,
      longHelp: `Returns a random pouting image.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['pouts'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    let mention = msg.mentions.members.first();
   
    nep.wolke.getRandom({
        type: 'pout',
        filetype: 'gif'
      }).then((res) => {
        return msg.channel.send({
          embed: new nep.discord.MessageEmbed()
            .setDescription(`Hmph >:T`)
            .setImage(res.url)
            .setColor(nep.rColor)
        })
      }).catch((err) => {
        util.error(err.message, true);
      })
  }
}


module.exports = Pout;