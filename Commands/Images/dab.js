const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
class Dab extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `<o/`,
      longHelp: `Dab on them haters.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['dab on the haters'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    nep.wolke.getRandom({type: 'dab'}).then((res) => {
    return msg.channel.send({
      embed: new nep.discord.MessageEmbed()
        .setDescription(`<o/`)
        .setImage(res.url)
        .setColor(nep.rColor)
    });
  }).catch((err) => {
      util.error(err.message, true);
    })

  }
}

module.exports = Dab;