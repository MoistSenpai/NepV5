const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
class Insult extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Insult a poor person!`,
      longHelp: `Returns a random anime gif of insults and stuff.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['insults'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

   run(msg, util, args, nep) {
    if (!args[0]) return calledForUser(`This is for you ${msg.author}...`);

    util.listUsers(args.join(' ')).then((user) => {
      if (user == msg.author) return calledForUser(`No one to hug? **Here you go** ${msg.author} ;3`);
      else if (user == nep.user) return calledForUser(`${msg.author} insulted me :c`);
      else return calledForUser(`${user} **this is from** ${msg.author}~`);
    }).catch((err) => util.error(err));

    function calledForUser(prompt) {
      nep.wolke.getRandom({
      type: 'insult',
      filetype: 'gif'
    }).then((res) => {
      return msg.channel.send({
        embed: new nep.discord.MessageEmbed()
          .setDescription(prompt)
          .setImage(res.url)
          .setColor(nep.rColor)
      })
    }).catch((err) => {
      util.error(err.message, true);
    })
    }
  }
}


module.exports = Insult;
