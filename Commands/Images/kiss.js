const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
class Kiss extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Kiss someone!`,
      longHelp: `Returns a kissing anime gif on someone you mention.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['kisses'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

   run(msg, util, args, nep) {
    if (!args[0]) return msg.channel.send({
     embed: new nep.discord.MessageEmbed()
       .setDescription(`**No one to kiss?** Too bad!`)
       .setImage(`https://i.kym-cdn.com/photos/images/newsfeed/000/926/994/729.gif`)
       .setColor(nep.rColor)
   });

    util.listUsers(args.join(' ')).then((user) => {
      if (user == msg.author) return msg.channel.send({
       embed: new nep.discord.MessageEmbed()
         .setDescription(`**No one to kiss?** Too bad!`)
         .setImage(`https://i.kym-cdn.com/photos/images/newsfeed/000/926/994/729.gif`)
         .setColor(nep.rColor)
     });
      else if (user == nep.user) return calledForUser(`I got **kissed by** ${msg.author} ...gross...`);
      else return calledForUser(`${user} **got kissed by** ${msg.author} owo`);
    }).catch((err) => util.error(err));

    function calledForUser(prompt) {
      nep.wolke.getRandom({
      type: 'kiss',
      filetype: 'gif'
    }).then((res) => {
      return msg.channel.send({
        embed: new nep.discord.MessageEmbed()
          .setDescription(prompt)
          .setImage(res.url)
          .setColor(nep.rColor)
      })
    }).catch((err) => {
      util.error(err.message, true);
    })
    }
  }
}


module.exports = Kiss;
