const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Cat extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Posts meow meows.`,
      longHelp: `Returns pictures of cats`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['kat', 'meow'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    msg.channel.send({
      embed: new nep.discord.MessageEmbed()
        .setDescription(`<:Nekogear:390652486546882588> ***Meow~!***`)
        .setImage(`http://thecatapi.com/api/images/get?format=src&size=full&timestamp=${new Date().valueOf()}`)
        .setColor(nep.rColor)
    });
  }
}

module.exports = Cat;
