const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
class Poke extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Poke a pet!`,
      longHelp: `Returns a random anime gif of poking someone.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['pokes'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

   run(msg, util, args, nep) {
    if (!args[0]) return calledForUser(`**Pooooke** ${msg.author}!`);

    util.listUsers(args.join(' ')).then((user) => {
      if (user == msg.author) return calledForUser(`**Pooooke** ${msg.author}!`);
      else if (user == nep.user) return calledForUser(`**Stop it** ${msg.author} >:c`);
      else return calledForUser(`**Pooooke** ${msg.author} from ${msg.author}!`);
    }).catch((err) => util.error(err));

    function calledForUser(prompt) {
      nep.wolke.getRandom({
      type: 'poke',
      filetype: 'gif'
    }).then((res) => {
      return msg.channel.send({
        embed: new nep.discord.MessageEmbed()
          .setDescription(prompt)
          .setImage(res.url)
          .setColor(nep.rColor)
      })
    }).catch((err) => {
      util.error(err.message, true);
    })
    }
  }
}


module.exports = Poke;
