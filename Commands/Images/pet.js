const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
class Pet extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Pet some random people!`,
      longHelp: `Returns a random anime gif of petting someone.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['pets', 'petting'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

   run(msg, util, args, nep) {
    if (!args[0]) return calledForUser(`Aww~! **No one to pet**? Here you go ${msg.author}!`);

    util.listUsers(args.join(' ')).then((user) => {
      if (user == msg.author) return calledForUser(`Aww~! **No one to pet**? Here you go ${msg.author}!`);
      else if (user == nep.user) return calledForUser(`I got a **pat from** ${msg.author} x3`);
      else return calledForUser(`${user} **got a pat from** ${msg.author}~!`);
    }).catch((err) => util.error(err));

    function calledForUser(prompt) {
      nep.wolke.getRandom({
      type: 'pat',
      filetype: 'gif'
    }).then((res) => {
      return msg.channel.send({
        embed: new nep.discord.MessageEmbed()
          .setDescription(prompt)
          .setImage(res.url)
          .setColor(nep.rColor)
      })
    }).catch((err) => {
      util.error(err.message, true);
    })
    }
  }
}


module.exports = Pet;
