const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class NSFW extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Enable and disable NSFW.`,
      longHelp: `Chose which channels can use NSFW command.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <-add, -remove> <Channel>`, `• ${nep.prefix}${path.basename(__filename, '.js')} <-list>`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} -add LewdCentral`, `• ${nep.prefix}${path.basename(__filename, '.js')} -remove NotLewd`, `• ${nep.prefix}${path.basename(__filename, '.js')} -list`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 3e3,
      aliases: [],
      locked: false,
      allowDM: false
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return msg.channel.send({
      embed: new nep.discord.MessageEmbed()
      .setDescription(`:x: | Woh dud, you **need the correct args**! Do \`${nep.prefix }help ${path.basename(__filename, '.js')}\` for more info!`)
      .setColor(nep.rColor)
    });
    if (args[0].toLowerCase() !== '-list' && args[0].toLowerCase() !== '-add' && args[0].toLowerCase() !== '-remove') return msg.channel.send({
      embed: new nep.discord.MessageEmbed()
      .setDescription(`:x: | No, you must have \`-add, -remove, or -list\`! Do \`${nep.prefix }help ${this.info.name}\` for more info!`)
      .setColor(nep.rColor)
    });
    if (args[0].toLowerCase() == '-list') return nsfw();

    util.listChannels(args.slice(1).join(' ')).then((channel) => {
      nsfw(channel);
    }).catch((err) => util.error(err));

    function nsfw(channel) {
      let alreadyAdded = [];

       if (args[0].toLowerCase() == '-add') { // Add channel <:Heven:390652489835347978>
         if (!channel.nsfw) return msg.channel.send({
          embed: new nep.discord.MessageEmbed()
            .setDescription(`:x: | Please **enable NSFW** in the channel settings, then try again!`)
            .setImage(`https://i.imgur.com/ilJAToP.png`)
            .setColor(nep.rColor)
         });

        nep.connection.query(`SELECT * FROM nsfw WHERE guildId = ${msg.guild.id}`, function(err, row) {
          if (row.length <= 0) {
            nep.connection.query(`INSERT INTO nsfw (guildId, channel) VALUES (?, ?) ON DUPLICATE KEY UPDATE channel = ${channel.id}`, [msg.guild.id, channel.id]);
            return msg.channel.send({embed: new nep.discord.MessageEmbed()
              .setDescription(`<:Heven:390652489835347978> | NSFW is now **enabled** for ${channel}!`)
              .setColor(nep.rColor)
            });
          }

          for (let i = 0; i < row.length; i++) {
            alreadyAdded.push(row[i]['channel']);
          }

          if (alreadyAdded.indexOf(channel.id) >= 0) return msg.channel.send({embed: new nep.discord.MessageEmbed()
            .setDescription(`:x: | I'm **already lewd on this channel**. What do you want from me?!`)
            .setColor(nep.rColor)
          });

          nep.connection.query(`INSERT INTO nsfw (guildId, channel) VALUES (?, ?) ON DUPLICATE KEY UPDATE channel = ${channel.id}`, [msg.guild.id, channel.id]);
          return msg.channel.send({embed: new nep.discord.MessageEmbed()
            .setDescription(`<:Heven:390652489835347978> | NSFW is now **enabled** for ${channel}!`)
            .setColor(nep.rColor)
          });
        });
      }
      else if (args[0].toLowerCase() == '-remove') { // Remove channel <:Fish:390652481404796928>
        nep.connection.query(`SELECT * FROM nsfw WHERE guildId = ${msg.guild.id}`, function(err, row) {
          if (row.length <= 0) return msg.channel.send({embed: new nep.discord.MessageEmbed()
            .setDescription(`:x: | I'm **not even lewd on any channel**, go away!`)
            .setColor(nep.rColor)
          });

          for (let i = 0; i < row.length; i++) {
            alreadyAdded.push(row[i]['channel']);
          }
          if (alreadyAdded.indexOf(channel.id) <= -1) return msg.channel.send({embed: new nep.discord.MessageEmbed()
            .setDescription(`:x: | I **wasn't even lewd on that channel**, leave me alone!`)
            .setColor(nep.rColor)
          });
          nep.connection.query(`DELETE FROM nsfw WHERE guildId = ${msg.guild.id} AND channel = ${channel.id}`);
          msg.channel.send({embed: new nep.discord.MessageEmbed()
            .setDescription(`<:Fish:390652481404796928> | NSFW is now **disabled** for ${channel}!`)
            .setColor(nep.rColor)
          });
        });
      }
      else if (args[0].toLowerCase() == '-list') { // List channels
        let ignoringRn = [];
        let seperatedList = [];
        let ignoreList;

        nep.connection.query(`SELECT * FROM nsfw WHERE guildId = ${msg.guild.id}`, function(err, row) {
          if (row.length <= 0) return msg.channel.send({embed: new nep.discord.MessageEmbed()
            .setDescription(`*You can't list something if there's nothing to list <:WeSmart:421536576707887114>*`)
            .setColor(nep.rColor)
          });

          for (let i = 0; i < row.length; i++) {
            ignoringRn.push(`<#${row[i]['channel']}>`);
          }
          ignoreList = ignoringRn.map((m) => m).join(' | ');

          if (ignoreList.length >= 1024) {
            while (ignoringRn.length) { // While reactions in arr split and join new line for seperated arr
              seperatedList.push(ignoringRn.splice(0, 10).join('\n')); // 1-x list
            }

            msg.channel.send({embed: new nep.discord.MessageEmbed()
              .setDescription(seperatedList[0])
              .setColor(nep.rColor)
            }).then(async (toReact) => { // Send first page
              await toReact.react('◀'); // React Left
              await toReact.react('▶'); // React right

              // Set up reaction collector
              const collector = toReact.createReactionCollector((r) => r.users.last().id == msg.author.id, {time: 60000});

              let counter = 0; // Page counter

              collector.on('collect', (r) => {
                if (r.emoji.name == '◀' && counter >= 0) {
                  counter-- // Minus page
                  if (counter == -1) counter = 0;
                  toReact.edit({embed: new nep.discord.MessageEmbed()
                    .setDescription(`**| NSFW Enabled |**\n\n${seperatedList[counter]}`)
                    .setColor(nep.rColor)
                  }); // Show page
                }
                if (r.emoji.name == '▶' && counter < seperatedList.length -1) {
                  counter++ // Add page
                  toReact.edit({embed: new nep.discord.MessageEmbed()
                    .setDescription(`**| NSFW Enabled |**\n\n${seperatedList[counter]}`)
                    .setColor(nep.rColor)
                  }); // Show page
                }
              });
              collector.on('remove', (r) => {
                if (r.emoji.name == '◀' && counter >= 0) {
                  counter-- // Minus page
                  if (counter == -1) counter = 0;
                  toReact.edit({embed: new nep.discord.MessageEmbed()
                    .setDescription(`**| NSFW Enabled |**\n\n${seperatedList[counter]}`)
                    .setColor(nep.rColor)
                  }); // Show page
                }
                if (r.emoji.name == '▶' && counter < seperatedList.length -1) {
                  counter++ // Add page
                  toReact.edit({embed: new nep.discord.MessageEmbed()
                    .setDescription(`**| NSFW Enabled |**\n\n${seperatedList[counter]}`)
                    .setColor(nep.rColor)
                  }); // Show page
                }
              });
            });
          } else {
            return msg.channel.send({embed: new nep.discord.MessageEmbed()
              .setDescription(`**| NSFW Enabled |**\n\n${ignoreList}`)
              .setColor(nep.rColor)
            });
          }
        });
      }
    }
  }
}

module.exports = NSFW;
