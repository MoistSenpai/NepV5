const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class R34 extends Command {
    constructor(nep) {
        super(nep, {
            name: path.basename(__filename, '.js'),
            help: `Rule34 stuff.`,
            longHelp: `Returns picstures from rule34!`,
            usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <Thing>`],
            examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} ass`],
            category: path.dirname(__filename).split(path.sep).pop(),
            cooldown: 1e3,
            aliases: ['rule34'],
            locked: false,
            allowDM: true
        });
    }

    // ---------------------------------------------------------------------------

    run(msg, util, args, nep) {
        if (!args[0]) return util.embed(`:x: | Woh dud, you **need the correct args**! Do \`${nep.prefix}help ${this.info.name}\` for more info!`);
        else if (!msg.guild) return search(args.join(' '));

        util.selectAll('nsfw', 'channel').then((row) => {
            if (row.length <= 0) return util.embed(`:no_good: | You Lewdy McLewderson, \`NSFW isn't enabled\` here, do \`${nep.prefix}nsfw\`!`);
            return search(args.join(' '));
        }).catch((err) => util.error(err));

        function search(searchy) {
            const booru = require('booru');

            booru.search('r34', [searchy], { limit: 1, random: true }).then((imgs) => {
                if (imgs.length == 0)
                    return msg.channel.send({
                        embed: new nep.discord.MessageEmbed()
                            .setDescription(`<:LyingDown:423577897412984832> | No images found for tag \`${util.parseArgs(args.join(' '))}\`!\n\`\`\`css\nHere is how to do tags for dummies:\n\nFor characters: \nNeptune becomes neptune_(neptunia)\nDawn becomes dawn_(pokemon)\n\nFor series:\nNeptunia becomes neptunia_(series)\nPokemon becomes: pokemon_(series)\n\nGeneral rules:\nInstead of spaces, use '_'\nblue hair becomes blue_hair\`\`\``)
                            .setColor(nep.rColor)
                    });
                for (let i of imgs) {
                    return msg.channel.send({
                        embed: new nep.discord.MessageEmbed()
                            .setDescription(`[Direct Link](${i.common.file_url})`)
                            .setImage(i.common.file_url)
                            .setFooter(msg.author.tag, msg.author.displayAvatarURL({ size: 2048 }))
                            .setColor(nep.rColor)
                    });
                }
            });

        }

    }
}

module.exports = R34;
