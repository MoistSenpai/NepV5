const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Sex extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Sex pics.`,
      longHelp: `Returns picstures of sex!`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} [tag]`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} yoga`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: [],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    const Pornsearch = require('pornsearch');
    const Searcher = new Pornsearch(args[0] ? args.join(' ') : 'sex', 'sex');

    Searcher.gifs().then((gifs) => {
      if (gifs.length < 0) return util.embed(`:x: | **Nothing** could be found for your tags.`);

      let randomUrl = gifs[Math.floor(Math.random() * gifs.length)].url;

      msg.channel.send({
        embed: new nep.discord.MessageEmbed()
        .setDescription(`[Direct Link](${randomUrl})`)
        .setColor(nep.rColor)
        .setImage(randomUrl)
        .setFooter(`${msg.author.tag} | Tag: ${util.parseArgs(args.join(' • '), 500)}`, msg.author.displayAvatarURL())
      });
    }).catch((err) => {
      if (err.message.startsWith('No results for search related'))
        return util.embed(`:x: | **No results** found for: \`${util.parseArgs(args[0])}\``);
      return util.error(err);
    });
  }
}

module.exports = Sex;
