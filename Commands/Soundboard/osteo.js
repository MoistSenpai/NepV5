const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');
const ytdl = require('ytdl-core');

class Osteo extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Play I have Osteoporosis.`,
      longHelp: `Joins VC and plays 'I have Osteoporosis'.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 5e3,
      aliases: [],
      /*['ping2']*/
      locked: false,
      allowDM: false
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    let voiceConnection = msg.guild.voiceConnection;
    let queue = util.getQueue(msg.guild.id);
    let vc = msg.member.voice.channel;

    if (queue && voiceConnection) return util.embed(`:x: | There is already something playing!`);
    else if (!msg.member.voice.channel) return util.embed(`:x: | Get in a **voice channel** cyka! *Self destruct in 5 seconds*`).then((m) => m.delete({timeout: 5e3})).catch((err) => util.error(`Error when deleteing my message:\n\n${err}`));

     msg.member.voice.channel.join().then((connection) => {
        let dispatcher = connection.play(ytdl(`https://www.youtube.com/watch?v=dKT2CdxeIFk`, {
          filter: 'audioonly'
        }));

       dispatcher.on('end', () => {
         if (!vc) return;
         else return vc.leave();
       });

     }).catch((err) => util.error(`Error when joining vc:\n\n${err}`));
  }

}

module.exports = Osteo;