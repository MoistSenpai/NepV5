const path = require(`path`);
const stripIndents = require('common-tags').stripIndents;

function clean(text) {
    if (typeof(text) === 'string') return text.replace(/`/g, '`' + String.fromCharCode(8203)).replace(/@/g, '@' + String.fromCharCode(8203));
    else return text;
}

module.exports.run = (client, msg, args) => {
  let input = args.join(' ');

  if (!args[0]) return msg.channel.send(`No args tho`);

  try {
    let evaled = eval(args.join(' '));

    if (evaled.toString().indexOf(client.token) >= 0) evaled = clean(evaled.toString().replace(client.token, 'What token? I\'m human just like you!')).replace(client.config.mysql.password, 'No password here xd');
    if (typeof evaled !== 'string') evaled = require('util').inspect(evaled);
    if (evaled.length > 1500) return msg.channel.send(`Msg too big fix later.`);

    sendEval(input, evaled);

  } catch (err) {
    sendEval(input, err, `:x:`)
  }

  function sendEval(input, output, emote) {
    msg.delete({timeout: 1000});

    if (!emote) emote = `✅`;
    msg.channel.send({embed: new client.discord.MessageEmbed()
      .setDescription(stripIndents`**Input:**\n\`\`\`js\n${input}\n\`\`\`${emote}**Output:**\n\`\`\`js\n${output}\n\`\`\``)
      .setColor(0xff8d14)
    });
  }
}

module.exports.info = {
  name: path.basename(__filename, '.js'),
  category: path.dirname(__filename).split(path.sep).pop(),
  help: `Evaluate some bitcoins`,
  longHelp: `Determines the current value of bitcoin cryptocurrency to determine if bot should buy more stocks or sell stocks during that given period of time.`,
  example: `${path.basename(__filename, '.js')}`,
  locked: false
}
