const path = require(`path`);

module.exports.run = (client, msg, args) => {
  if (!args[0]) return msg.channel.send(`Need args tho`);

  client.connection.query(`${args.join(' ')}`, function(err, row, field) {
    if (err) return msg.channel.send(err, {code: 'css'});
    return msg.channel.send({
      embed: new client.discord.MessageEmbed()
      .setDescription(`👌 | Query: \`${args.join(' ')}\``)
      .setColor(client.rColor)
    });
  });

}

module.exports.info = {
  name: path.basename(__filename, '.js'),
  category: path.dirname(__filename).split(path.sep).pop(),
  help: `Do SQL stuff.`,
  longHelp: `Run a SQL query to the database.`,
  example: `${path.basename(__filename, '.js')} <Args>`,
  locked: false
}
