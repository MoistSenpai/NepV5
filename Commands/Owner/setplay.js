const path = require('path');

module.exports.run = (client, msg, args) => {
    if (!args[0]) return msg.channel.send(`No args boi`);

    msg.channel.send({
        embed: new client.discord.MessageEmbed()
            .setDescription(`🇵 \`Playing\` **${args.join(' ')}**\n🇸 \`Streaming\` **${args.join(' ')}**\n🇱 \`Listening\` **${args.join(' ')}**\n🇼 \`Watching\` **${args.join(' ')}**`)
            .setColor(client.rColor)
    }).then(async (mm) => {
        const collector = new client.discord.ReactionCollector(mm, m => m.users.last().id == msg.author.id, {
            time: 3e4
        });

        await mm.react('🇵');
        await mm.react('🇸');
        await mm.react('🇱');
        await mm.react('🇼');

        collector.on('collect', (m) => {
            if (m.emoji.name == '🇵') return client.user.setActivity(args.join(' '), {
                    type: 'PLAYING'
                }).then(() => mm.edit({
                    embed: new client.discord.MessageEmbed().setDescription(`**PLAYING** set to \`${args.join(' ')}\``).setColor(client.rColor)
                }))
                .then(() => collector.stop()).then(() => mm.reactions.removeAll());
            else if (m.emoji.name == '🇸') return client.user.setActivity(args.join(' '), {
                    type: 'STREAMING'
                }).then(() => mm.edit({
                    embed: new client.discord.MessageEmbed().setDescription(`**STREAMING** set to \`${args.join(' ')}\``).setColor(client.rColor)
                }))
                .then(() => collector.stop()).then(() => mm.reactions.removeAll());
            else if (m.emoji.name == '🇱') return client.user.setActivity(args.join(' '), {
                    type: 'LISTENING'
                }).then(() => mm.edit({
                    embed: new client.discord.MessageEmbed().setDescription(`**LISTENING** set to \`${args.join(' ')}\``).setColor(client.rColor)
                }))
                .then(() => collector.stop()).then(() => mm.reactions.removeAll());
            else if (m.emoji.name == '🇼') return client.user.setActivity(args.join(' '), {
                    type: 'WATCHING'
                }).then(() => mm.edit({
                    embed: new client.discord.MessageEmbed().setDescription(`**WATCHING** set to \`${args.join(' ')}\``).setColor(client.rColor)
                }))
                .then(() => collector.stop()).then(() => mm.reactions.removeAll());
        });
    });
}

module.exports.info = {
    name: path.basename(__filename, '.js'),
    category: path.dirname(__filename).split(path.sep).pop(),
    help: `Set stuff.`,
    longHelp: `Sets stuff to bot and stuff.`,
    example: `${path.basename(__filename, '.js')} <Stuff>`,
    locked: false
}