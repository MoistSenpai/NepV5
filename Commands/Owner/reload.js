const path = require(`path`);

module.exports.run = (client, msg, args) => {
  if (!args[0]) return msg.channel.send(`No args yo`);
  
  // Reload Functions, Commands, and Events
  
  if (args.join(' ') in client.functions) return msg.channel.send({embed: new client.discord.MessageEmbed().setDescription(`<:MoistNotBad:434831729753063445> | The **function** \`${args.join(' ')}\` has been reloaded!`).setColor(client.rColor)})
                                                 .then(() => client.loadFunctions(client, args.join(' ')));
  if (args.join(' ') in client.commands) return msg.channel.send({embed: new client.discord.MessageEmbed().setDescription(`<:MoistNotBad:434831729753063445> | The **command** \`${args.join(' ')}\` has been reloaded!`).setColor(client.rColor)})
                                                 .then(() => client.loadCommands(client, args.join(' ')));
  if (args.join(' ') in client.events) return msg.channel.send({embed: new client.discord.MessageEmbed().setDescription(`<:MoistNotBad:434831729753063445> | The **event** \`${args.join(' ')}\` has been reloaded!`).setColor(client.rColor)})
                                                 .then(() => client.loadEvents(client, args.join(' ')));
  
};

module.exports.info = {
  name: path.basename(__filename, '.js'),
  category: path.dirname(__filename).split(path.sep).pop(),
  help: `Quick nap.`,
  longHelp: `Refresh my mind from all the hard anal I've been getting master(s).`,
  example: `${path.basename(__filename, '.js')} <Command, Event, or Function>`,
  locked: false
}