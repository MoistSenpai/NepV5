const path = require(`path`);
const snekfetch = require('snekfetch');

module.exports.run = (client, msg, args) => {
    let key = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjMxMzE1NzIzOTcxNDIxNzk4NCIsImJvdCI6dHJ1ZSwiaWF0IjoxNTE0NzYxODE5fQ.Un3ZR7gXHQGF_uCRZmzI1P3yGe7JP63rocvN4MCkzWY';
    let key2 = 'd6bfabad5bb7a57b45f00c9e86d2583d032da1bc6006c09c61a1f326b368a4d6f116d951fb66a664fdba39cb51483d2c60a0466f7c675b4f106cd1f4f08e905e';

    snekfetch.post(`https://discordbots.org/api/bots/${client.user.id}/stats`)
        .set('Authorization', key)
        .send({
            server_count: client.guilds.size
        }).then(() => {
            msg.channel.send(`*K done* \`(1)\``).then((m) => {
                snekfetch.post(`https://botsfordiscord.com/api/v1/bots/${client.user.id}`)
                    .set(`Content-type`, `application/json`)
                    .set('Authorization', key2)
                    .send({
                        server_count: client.guilds.size
                    }).then(() => m.edit(`*K done* \`(Both)\``));
            });
        });
}

module.exports.info = {
    name: path.basename(__filename, '.js'),
    category: path.dirname(__filename).split(path.sep).pop(),
    help: `Tests`,
    longHelp: `Tests stuff`,
    example: `${path.basename(__filename, '.js')}`,
    locked: false
}