const path = require('path');
const mysql = require('mysql');

module.exports.run = (client, msg, args) => {
  msg.channel.send(`It is done...`);
}

module.exports.info = {
    name: path.basename(__filename, '.js'),
    category: path.dirname(__filename).split(path.sep).pop(),
    help: `;3`,
    longHelp: `>:3`,
    example: `${path.basename(__filename, '.js')}`,
    locked: false
}
