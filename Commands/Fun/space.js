const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Space extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `S p a c e.`,
      longHelp: `S p a c e s  o u t  s t u f f.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <Text>`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} Hello yes`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: [],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`:x: | I give **something** to space though!`);

    let amount = 2;

    return util.send(args.join(' '.repeat(amount / 2)).split('').join(' '.repeat(amount))).catch((err) => util.error(err));
  }
}

module.exports = Space;
