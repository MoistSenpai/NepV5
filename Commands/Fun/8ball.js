const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Ping extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Magic Nep Ball!.`,
      longHelp: `'Magic' 8 ball game thing.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <Question>`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} Does my waifu love me?`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: [],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`:x: | I don't have **anything to use** my magic on!`);

    util.send(`*Working magic...*`).then((m) => {
      setTimeout(() => {
        let options = [
            'Yeah, sure',
            'Who knows? Not me',
            'Obviously yes',
            'Obviously no',
            'More \'yes\' than yes itself',
            'Yes',
            'No',
            'Fricky frack, no'
        ];

        return util.embed(`<a:PenguinDance:429390316261605396> | My **answer** is \`${options[Math.floor(Math.random() * options.length)]}\`! :crystal_ball:`, m);

      }, 1e3);
    });
  }
}

module.exports = Ping;
