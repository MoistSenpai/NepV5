const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class DadJoke extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Jokes of the dads.`,
      longHelp: `Returns dad jokes.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['djoke', 'dad'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    let options = {
      headers: {
        'Accept': 'application/json'
        }
      }

    curl.getJSON(`https://icanhazdadjoke.com/`, options, (err, resp, data) => {
      if (err) return msg.channel.send(err, {code: 'css'});
      return util.embed(`👴 | *${data.joke}*`);
    });

  }
}

module.exports = DadJoke;
