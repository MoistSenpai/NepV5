const path = require(`path`);

module.exports.run = (client, msg, args) => {

    var arry = msg.guild.emojis.map(x => x.toString());
    var embedList = [];
    var positiony = 0;

      function loopy (arr) {
        if (arr.length >= 25) {
          embedList.push(new client.discord.MessageEmbed().setDescription(`${arr.slice(0, 5).join(" ")}\n${arr.slice(5, 10).join(" ")}\n${arr.slice(10, 15).join(" ")}\n${arr.slice(15, 20).join(" ")}\n${arr.slice(20, 25).join(" ")}\n`).setColor(client.rColor).setFooter(`Total Emojis: ${arry.length}`, msg.author.avatarURL()))
          loopy(arr.slice(25, arr.length))
        }
        else {
          if (arr.length > 20) {
            embedList.push(new client.discord.MessageEmbed().setDescription(`${arr.slice(0, 5).join(" ")}\n${arr.slice(5, 10).join(" ")}\n${arr.slice(10, 15).join(" ")}\n${arr.slice(15, 20).join(" ")}\n${arr.slice(20, arr.length).join(" ")}\n`).setColor(client.rColor).setFooter(`Total Emojis: ${arry.length}`, msg.author.avatarURL()))
          }
          else if (arr.length > 15) {
            embedList.push(new client.discord.MessageEmbed().setDescription(`${arr.slice(0, 5).join(" ")}\n${arr.slice(5, 10).join(" ")}\n${arr.slice(10, 15).join(" ")}${arr.slice(15, arr.length).join(" ")}\n`).setColor(client.rColor).setFooter(`Total Emojis: ${arry.length}`, msg.author.avatarURL()))
          }
          else if (arr.length > 10) {
            embedList.push(new client.discord.MessageEmbed().setDescription(`${arr.slice(0, 5).join(" ")}\n${arr.slice(5, 10).join(" ")}\n${arr.slice(10, arr.length).join(" ")}`).setColor(client.rColor).setFooter(`Total Emojis: ${arry.length}`, msg.author.avatarURL()))
          }
          else if (arr.length > 5) {
            embedList.push(new client.discord.MessageEmbed().setDescription(`${arr.slice(0, 5).join(" ")}\n${arr.slice(5, arr.length).join(" ")}`).setColor(client.rColor).setFooter(`Total Emojis: ${arry.length}`, msg.author.avatarURL()))
          }
          else if (arr.length > 0) {
            embedList.push(new client.discord.MessageEmbed().setDescription(`${arr.slice(0, arr.length).join(" ")}`).setColor(client.rColor).setFooter(`Total Emojis: ${arry.length}`, msg.author.avatarURL()))
          }
        }
      }


      loopy(arry);
      // console.log(embedList);
      msg.channel.send("Loading...").then(async (toReact) => {
      await toReact.react("◀");
      await toReact.react("🌎");
      await toReact.react("▶");
      toReact.edit(`${positiony+1} of ${embedList.length}`, {embed: embedList[positiony]});
      // console.log(embedList[4]);
      var collector =  new client.discord.ReactionCollector(toReact, m => m.users.last().id == msg.author.id, {time: 60000, dispose: true});
      collector.on('collect', (mm) => {
        runLRRfunc(mm);
      });
      collector.on('remove', (mm) => {
        runLRRfunc(mm);
      });

      function runLRRfunc(mm) {
        if (mm.emoji.name == "◀") {
          runEdit("lefty");
        } else if (mm.emoji.name == "▶") {
          runEdit("righty");
        } else if (mm.emoji.name == "🌎") {
          runEdit("resety");
        }
      }

      function runEdit(vary) {
        if (vary == "lefty" && positiony > 0) {
          positiony--;
          toReact.edit(`${positiony+1} of ${embedList.length}`,{embed: embedList[positiony]});
        }
        if (vary == "righty" && (positiony < embedList.length - 1)) {
          positiony++;
          toReact.edit(`${positiony+1} of ${embedList.length}`,{embed: embedList[positiony]});
        }
        if (vary == "resety") {
          positiony = 0;
          toReact.edit(`${positiony+1} of ${embedList.length}`,{embed: embedList[positiony]});
        }
      }

    })

}

module.exports.info = {
  name: path.basename(__filename, '.js'),
  category: path.dirname(__filename).split(path.sep).pop(),
  help: `A list of faces`,
  longHelp: `I'll show you all of my emoji faces, 1 by 1.`,
  example: `${path.basename(__filename, '.js')} <message>`,
  locked: false
}
