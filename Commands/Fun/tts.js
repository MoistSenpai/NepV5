const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class TTS extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `TTS text.`,
      longHelp: `Text to speach text.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <Text>`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} Funny stuff`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: [],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`:x: | I give **something** to tts though!`);
    else if (!util.hasPermission('SEND_TTS_MESSAGES')) return util.hasPermission('SEND_TTS_MESSAGES', true);

    return msg.channel.send(args.join(' '), {tts: true});

  }
}

module.exports = TTS;
