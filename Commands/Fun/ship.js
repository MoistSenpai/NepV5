const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Ship extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `I'll judge your ships.`,
      longHelp: `I'll give you a compatible rating of your ships.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <Ship1> and <Ship2>`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} Neptune and Noire`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: [],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    let split = args.join(' ').toLowerCase().split(' ').indexOf(`and`);
    let chance = randomNumb(0, 100);
    let firstArray = [];
    let secondArray = [];

    let mention = msg.mentions.members.first();
    let mention2 = msg.mentions.members.array()[1];

    for (let i = 0; i < split; i++) {
      if (mention) {
        firstArray.push(mention.user.username);
      }
      else {
        firstArray.push(args[i]);
      }
    }
    for (let i = split + 1; i <= args.length; i++) {
      if (mention2) {
        secondArray.push(mention2.user.username);
        break;
      }
      else {
        secondArray.push(args[i]);
      }
    }
    let FirstPick = firstArray.join(' ');
    let SecondPick = secondArray.join(' ');

    if (!args[0]) return util.embed(`:x: | Give me some **ships to sail** or sink! >:3`);
    if (split <= 0) return util.embed(`:x: | Divide your ships with an \`and\`!\n\`\`\`css\n${nep.prefix}${this.info.name} Nep and Vert\n\`\`\``);

    return util.embed(`\`${FirstPick}\` and \`${SecondPick}\`... **${chance}%** compatible ${symbol()}!`);

    function symbol() {
      if (chance >= 50) return `:heart:`;
      if (chance <= 50) return `:broken_heart:`;
    }

    function randomNumb(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min)) + min;
    }
  }
}

module.exports = Ship;
