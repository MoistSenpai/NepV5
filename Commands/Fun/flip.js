const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Flip extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Ⅎlᴉds your text!`,
      longHelp: `Returns the flipped varient of your text.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <Text>`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} Your're a fatty lmao`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: [],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`:x: | I give **something** to Ⅎlᴉd though!`);

    const mapping = '¡"#$%⅋,)(*+\'-˙/0ƖᄅƐㄣϛ9ㄥ86:;<=>?@∀qƆpƎℲפHIſʞ˥WNOԀQɹS┴∩ΛMX⅄Z[/]^_`ɐqɔpǝɟƃɥᴉɾʞlɯuodbɹsʇnʌʍxʎz{|}~';
    const offSet = '!'.charCodeAt(0);

    return util.send(args.join(' ').split('').map(c => c.charCodeAt(0) - offSet).map(c => mapping[c] || ' ').join('')).catch((err) => util.error(err));
  }
}

module.exports = Flip;
