const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Say extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Make me say stuff.`,
      longHelp: `Make me say stuff.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <Text>`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} Wasup`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['sayd'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`:x: | Give me **something** to say!`);
    
    msg.delete({timeout: 10}).catch((err) => err);
    return util.send(args.join(' ')).catch((err) => util.error(err));
  }
}

module.exports = Say;
