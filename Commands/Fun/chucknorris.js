const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class ChuckNorris extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Chuck Norris facts!`,
      longHelp: `Returns Chuck Norris facts from the ChuckNorris Database.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')}`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['cnorris', 'chuck', 'norris', 'pleasedonthurtmeihave3kids'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    msg.channel.startTyping();
    curl.getJSON('http://api.icndb.com/jokes/random', function (err, response, data) {
        if (err) return client.error(client, msg, err);
        util.embed(`<:ChuckNorris:429692536412700673> | *${data.value.joke}*`).then(() => msg.channel.stopTyping(true));
    });
  }
}

module.exports = ChuckNorris;
