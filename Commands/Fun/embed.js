const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Embed extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Embeds stuff.`,
      longHelp: `Returns string but embed and stuff.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <Text>`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} Funny stuff`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: [],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`:x: | I give **something** to embed though!`);
    return util.send(args.join(' ').split('').map(c => c.charCodeAt(0) - offSet).map(c => mapping[c] || ' ').join('')).catch((err) => util.error(err));
  }
}

module.exports = Embed;
