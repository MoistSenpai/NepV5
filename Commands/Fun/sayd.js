const path = require(`path`);

module.exports.run = (client, msg, args) => {
    if (!client.checkPerms(client, msg, 'MANAGE_MESSAGES')) return client.checkPerms.sendMessage(client, msg, 'MANAGE_MESSAGES');
    else if (!args[0]) return;

    msg.delete({timeout: 10}).catch((err) => client.error(client, msg, `I don't have perms to delte your command: \n${err.message}`));
    return msg.channel.send(args.join(' ')).catch((err) => client.error(client, msg, `${err.message}`));
}

module.exports.info = {
    name: path.basename(__filename, '.js'),
    category: path.dirname(__filename).split(path.sep).pop(),
    help: `Repeat after me. Delete edition.`,
    longHelp: `I'll say the phrase you would like me to say and I'll delte your command message.`,
    example: `${path.basename(__filename, '.js')} <message>`,
    locked: false
}