const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Clap extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `dOeS 👏 thIs 👏 sHiT.`,
      longHelp: `If you're happy and you know it...`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <Text>`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} Holy shit it's Leafy`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: [],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`👏 | Provide **some text** to clap and stuff.`);

    const randomizeCase = word => word.split('').map(c => Math.random() > 0.5 ? c.toUpperCase() : c.toLowerCase()).join('');
    return util.send(args.map(randomizeCase).join(' 👏 ')).catch((err) => util.error(err));

  }
}

module.exports = Clap;
const randomizeCase = word => word.split('').map(c => Math.random() > 0.5 ? c.toUpperCase() : c.toLowerCase()).join('');
const path = require('path');
const { MessageEmbed } = require('discord.js');

module.exports.run = (client, msg, args) => {
    if (args.length < 1) return msg.channel.send({
        embed: new MessageEmbed()
            .setDescription(`:x: | Provide :clap: some :clap: text :clap: to :clap: clap`)
            .setColor(client.rColor)
    })

    msg.channel.send(args.map(randomizeCase).join(' 👏 ')).catch((err) => client.error(client, msg, err.message));
};

module.exports.info = {
    name: path.basename(__filename, '.js'),
    category: path.dirname(__filename).split(path.sep).pop(),
    help: `:clap: Your text!`,
    longHelp: `Returns the 'clap' meme of your text.`,
    example: `${path.basename(__filename, '.js')} <Text>`,
    locked: false
}