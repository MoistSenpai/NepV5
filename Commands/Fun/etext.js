const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class EText extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Makes text emojis.`,
      longHelp: `Returns string but with emojis and stuff.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <Text>`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} You're fat`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['emojitext'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`:x: | I give **something** to emoji though!`);

    const mapping = {
        ' ': '   ',
        '0': ':zero:',
        '1': ':one:',
        '2': ':two:',
        '3': ':three:',
        '4': ':four:',
        '5': ':five:',
        '6': ':six:',
        '7': ':seven:',
        '8': ':eight:',
        '9': ':nine:',
        '!': ':grey_exclamation:',
        '?': ':grey_question:',
        '#': ':hash:',
        '*': ':asterisk:'
    };

    'abcdefghijklmnopqrstuvwxyz'.split('').forEach(c => {
        mapping[c] = mapping[c.toUpperCase()] = ` :regional_indicator_${c}:`;
    });

    let thing = msg.mentions.members.first() ? args.join(' ').replace(msg.mentions.members.first(), msg.mentions.members.first().user.username).split('').map((c) => mapping[c] || c).join('') : args.join(' ').split('').map((c) => mapping[c] || c).join('')
    return util.send(thing).catch((err) => util.error());

  }
}

module.exports = EText;
