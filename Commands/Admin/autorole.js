const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Autorole extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Give roles to new members.`,
      longHelp: `Automaticly asigns role(s) to new server member(s).`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <-add, remove, -list> <RoleID, Mention, Name>`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} -add CoolRole69`, `• ${nep.prefix}${path.basename(__filename, '.js')} -remove CoolRole69`, `• ${nep.prefix}${path.basename(__filename, '.js')} -list`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['arole'],
      locked: false,
      allowDM: false
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`:x: | Not enough arguments. Do \`${nep.prefix}help ${this.info.name}\` for more info.`);
    else if (!util.hasPermission('MANAGE_ROLES_OR_PERMISSIONS')) return util.hasPermission('MANAGE_ROLES_OR_PERMISSIONS', true);

    let flag = args[0].toLowerCase();
    let postFlag = args.slice(1).join(' ');
    let autoRoleClass = new (require(`${nep.dir}/Classes/autoRoleClass.js`))(msg, util, args, nep, flag, postFlag, this);

    switch (flag) {
      case '-add': return autoRoleClass.add();
      case '-remove': return autoRoleClass.remove();
      case '-list': return autoRoleClass.list();
      default: return util.embed(`:x: | Incorrect arguments. Do \`${nep.prefix}help ${this.info.name}\` for more info.`);
    }

  }
}

module.exports = Autorole;
