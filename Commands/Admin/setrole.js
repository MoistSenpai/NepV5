const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class SetRole extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Set roles for users to pick.`,
      longHelp: `Set role for users to pick and stuff.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <-add, -remove> <RoleID, Mention, Name>`,],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} -add BestRole420`, `• ${nep.prefix}${path.basename(__filename, '.js')} -remove WorstRole360`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: [],
      locked: false,
      allowDM: false
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!util.hasPermission('ADMINISTRAOR')) return util.hasPermission('ADMINISTRAOR', true);
    else if (!args[0]) return util.embed(`:x: | Woh dud, you **need the correct args**! Do \`${nep.prefix }help ${this.info.name}\` for more info!`)
    else if (args[0].toLowerCase() !== '-add' && args[0].toLowerCase() !== '-remove') return util.embed(`:x: | Woh dud, you **need the correct args**! Do \`${nep.prefix }help ${this.info.name}\` for more info!`)

    util.listRoles(args.slice(1).join(' ')).then((role) => {
      roley(role);
    }).catch((err) => util.error(err));

    function roley(role) {
        nep.connection.query(`SELECT * FROM setrole WHERE guildId = ${msg.guild.id}`, function (err, row) {
            if (args[0].startsWith(`-add`)) { // Add role
                if (!msg.guild.me.hasPermission(`MANAGE_ROLES`)) return util.embed(`:x: | I don't have the **permission** \`MANAGE_ROLES\` give pls!`);

                nep.connection.query(`INSERT INTO setrole (guildId, roleId) VALUES (?, ?) ON DUPLICATE KEY UPDATE roleId = ${role.id}`, [msg.guild.id, role.id]);

                for (let i = 0; i < row.length; i++) {
                    if (row[i]['roleId'] == role.id) return util.embed(`:x: | Leaf me alone, [${role}] is **already in** roles! >:T`);
                }
                return util.embed(`<:ThumbsUp:427532146140250124> | **Added** [${role}] to role list!`);
            }
            if (args[0].startsWith(`-remove`)) { // Remove role
                if (row.length <= 0) return util.embed(`:x: | Leave me alone, there **isn't even anything** in role list! >:c`);

                  let rolesy = [];

                  for (let i = 0; i < row.length; i++) { rolesy.push(row[i].roleId); }

                    if (rolesy.indexOf(role.id) < 0) return util.embed(`:x: | Hey stupid, [${role}] **isn't in** role list!`);

                    return util.embed(`<:LyingDown:423577897412984832> | **Removed** [${role}] from role list!`).then(() => {
                        nep.connection.query(`DELETE FROM setrole WHERE roleId = ${role.id} AND guildId = ${msg.guild.id}`);
                    });

            }
        });
    }
  }
}

module.exports = SetRole;
