const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');
const moment = require('moment');

class Ban extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Usual 'kick' command.`,
      longHelp: `Kicks a member on the guild`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <User ID, Mention, or Tag> -reason [Reason]`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} MoistHater609`, `• ${nep.prefix}${path.basename(__filename, '.js')} MoistHater609#7212`, `• ${nep.prefix}${path.basename(__filename, '.js')} 184157133187710977`, `• ${nep.prefix}${path.basename(__filename, '.js')} MoistHater609 -reason You're fat`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: [],
      locked: false,
      allowDM: false
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`:x: | Not enough arguments. Do \`${nep.prefix}help ${this.info.name}\` for more info.`);
    else if (!util.hasPermission('KICK_MEMBERS')) return util.hasPermission('KICK_MEMBERS', true);

    let seperated = args.join(' ').toLowerCase().split(' ').indexOf('-reason');
    let user = [];
    let toKick;
    let reasonRegEx = /(?<=-reason).*$/gmi;

    if (seperated >= 0) {
      for (let i = 0; i < seperated; i++) { user.push(args[i]); }
      toKick = user.join(' ');
    }
    else toKick = args.join(' ');
    let reason = args.join(' ').toLowerCase().indexOf('-reason') > -1 ? reasonRegEx.exec(args.join(' ')) : 'No reason';

    util.listUsers(toKick).then((u) => {
      let member = msg.guild.members.get(u.id);
      let kickedEmbed = {
        color: Math.floor(Math.random() * 16777215),
        author: {
          name: moment().format(`MMMM Do, YYYY`),
          icon_url: u.avatarURL({size: 2048})
        },
        fields: [
          {
            name: `👢 | Kick`,
            value: `**[${member}]** \`by\` **[${msg.author}]**`
          },
          {
            name: `📋 | Reason`,
            value: `\`${util.parseArgs(reason)}\``
          }
        ]
      };
      member.kick(reason !== undefined ? util.parseArgs(reason) : '').then((kicked) => {
        msg.channel.send({embed: kickedEmbed})
        u.send({embed: kickedEmbed}).catch((err) => err);
      }).catch((err) => util.error(`Error when kicking user:\n\n${err}`));


    }).catch((err) => util.error(err));

  }
}

module.exports = Ban;
