const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Ignore extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Ignore people, channels, and roles.`,
      longHelp: `Ignore commands from people, channels, or roles.`,
      usage: [
        `• ${nep.prefix}${path.basename(__filename, '.js')} <-channel, -role, -user> <-allow or -deny> <Argument>`,
        `• ${nep.prefix}${path.basename(__filename, '.js')} <-channel, -role, user> <-list>`,
        `\nCan replace -channel, -role, and user with -c, -r, -u`
      ],
      examples: [
        `• ${nep.prefix}${path.basename(__filename, '.js')} -channel -deny Fornite`,
        `• ${nep.prefix}${path.basename(__filename, '.js')} -channel -allow Not_Fortnite`,
        `• ${nep.prefix}${path.basename(__filename, '.js')} -user -deny ForniteLover69`,
        `• ${nep.prefix}${path.basename(__filename, '.js')} -user -allow Xx_Minecraft_Hunger_Games_xX`,
        `• ${nep.prefix}${path.basename(__filename, '.js')} -role -deny DumbRole22`,
        `• ${nep.prefix}${path.basename(__filename, '.js')} -role -allow GoodRole52`
      ],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: [],
      locked: false,
      allowDM: false
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    // Define argument parts
    let flag = args[0]; // -user etc.
    let suffix = args[1]; // -allow, -deny
    let postSuffix = args.slice(2); // Channel, user, or role

    // Handle no args ofc ofc
    if (!args[0]) return util.embed(`:x: | Not enough arguments. Do \`${nep.prefix}help ${this.info.name}\` for more info.`);
    else if (!util.hasPermission('ADMINISTRATOR')) return util.hasPermission('ADMINISTRATOR', true);

    // Ignore class
    const ignoreClass = new (require(`${nep.dir}/Classes/ignoreClass.js`))(msg, util, args, nep, flag, suffix, postSuffix);

    // Switch case for -channel, -role, -user
    switch(flag) {
      // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-channel':
        ignoreClass.channel();
      break;
      // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-role':
        ignoreClass.role();
      break;
      // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-user':
        ignoreClass.user();
      break;
      // -=-=-=--=-=-=-=-=-=-=-=-=-=
        case '-c':
        ignoreClass.channel();
      break;
      // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-r':
        ignoreClass.role();
      break;
      // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-u':
        ignoreClass.user();
      break;
      // -=-=-=--=-=-=-=-=-=-=-=-=-=
      default:
        util.embed(`:x: | Not enough arguments. Do \`${nep.prefix}help ${this.info.name}\` for more info.`);
      // -=-=-=--=-=-=-=-=-=-=-=-=-=
    }



  }
}

module.exports = Ignore;
