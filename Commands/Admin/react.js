const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class React extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Message reactions.`,
      longHelp: `Allows you to set custom 'reactions' to messages.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <-add, -include> <Msg> => <Reaction>`, `• ${nep.prefix}${path.basename(__filename, '.js')} <-list>`, `• ${nep.prefix}${path.basename(__filename, '.js')} <-remove> <ID>`, `• ${nep.prefix}${path.basename(__filename, '.js')} <-purge>`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} -add Hello => Hey there!`, `• ${nep.prefix}${path.basename(__filename, '.js')} -include XD => shut up\n• If any message has 'XD', it will send\n  Ex. 'Wasup XD' would send reaction`, `• ${nep.prefix}${path.basename(__filename, '.js')} -remove AQr31`, `• ${nep.prefix}${path.basename(__filename, '.js')} -list`, `• ${nep.prefix}${path.basename(__filename, '.js')} -prune`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['reaction', `reactions`, `reacts`, `r`],
      locked: false,
      allowDM: false
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`:x: | Not enough arguments. Do \`${nep.prefix}help ${this.info.name}\` for more info.`);
    else if (!util.hasPermission('MANAGE_MESSAGES')) return util.hasPermission('MANAGE_MESSAGES', true);

    let flag = args[0];
    let suff = args.slice(1);
    let sep = suff.indexOf('=>');

    let message = [];
    let reaction = [];

    for (let i = 0; i < sep; i++) { message.push(suff[i]); }
    for (let i = sep + 1; i <= suff.length; i++) { reaction.push(suff[i]); }

    let reactionClass = new (require(`${nep.dir}/Classes/ReactionClass.js`))(msg, util, args, nep, message.join(' '), reaction.join(' '), this, sep);

    switch (flag.toLowerCase()) {
      case '-add': return reactionClass.add();
      case '-include': return reactionClass.include();
      case '-remove': return reactionClass.remove();
      case 'prune':
      case '-purge': return reactionClass.purge();
      case '-list': return reactionClass.list();
      default: return util.embed(`:x: | Incorrect arguments. Do \`${nep.prefix}help ${this.info.name}\` for more info.`);
    }

    // return util.code(`css`, `Flag: ${flag}\nMsg: ${message.join(' ')}\nReaction: ${reaction.join(' ')}`);
  }
}

module.exports = React;
