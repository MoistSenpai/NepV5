const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');
const moment = require('moment');

class Unban extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Unbans someone.`,
      longHelp: `Unbans a member who was banned on the server,`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <ID [ID2, ID3 etc.]> `],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} 184157133187710977`, `• ${nep.prefix}${path.basename(__filename, '.js')} 184157133187710977 251091302303662080 etc.`,],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['uban'],
      locked: false,
      allowDM: true
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`:x: | Not enough arguments. Do \`${nep.prefix}help ${this.info.name}\` for more info.`);
    else if (!util.hasPermission('BAN_MEMBERS')) return util.hasPermission('BAN_MEMBERS', true);

    let toUnban = args.join(' ').replace(/,/g, ' ').replace(/\s+/g, ' ').split(' ');

    new Promise((resolve, reject) => {
      for (let id of toUnban) {
        msg.guild.members.unban(id).then((user) => {
          let users = [];
          users.push(user);

          let embed = {
            color: Math.floor(Math.random() * 16777215),
            author: {
              name: moment().format(`MMMM Do, YYYY`),
              icon_url: users[0].avatarURL({size: 2048})
            },
            fields: [
              {
                name: `🤝 | Unban(s)`,
                value: `**[${users.length > 3 ? `${users[0]} | ${users[1]} | ${users[2]}` : users.map((u) => u).join(' | ')}]** \`by\` **[${msg.author}]**`
              }
            ]
          };

          msg.channel.send({ embed: embed });
          user.send({ embed: embed }).catch((err) => err);
        }).catch((err) => reject(err));
      }
    }).catch((err) => {
      if (err.toString().startsWith('DiscordAPIError: Invalid Form Body')) return util.embed(`:x: | You must only **provide IDs** to unban!`);
      else if (err.toString().startsWith('DiscordAPIError: Unknown Ban')) return util.embed(`:x: | I cannot unban someone who **isn't on** the ban list!`);
      else return util.error(err);
    });

  }
}

module.exports = Unban;
