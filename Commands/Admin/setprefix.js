const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');

class Setprefix extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Sets my prefix for the server.`,
      longHelp: `Sets a new prefix to be recognized by me.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <Prefix>`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} >_<`, `• After this, future commands would look like:\n>_<ping`, `• Prefixes cannot be over 8 characters`, `• All spaces will be replaced with '-'`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['sprefix', 'prefixset'],
      locked: false,
      allowDM: false
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`:x: | Not enough arguments. Do \`${nep.prefix}help ${this.info.name}\` for more info.`);
    else if (!util.hasPermission('ADMINISTRATOR')) return util.hasPermission('ADMINISTRATOR', true);

    let prefix = args.join(' ').replace(/\s+/g, '-');

    if (prefix.length > 8) return util.embed(`:x: | Prefixes cannot be over **8 characters** long! Try again.`);

    nep.connection.query(`UPDATE servers SET prefix = \'${prefix}\' WHERE guildId = ${msg.guild.id}`, function(err) {
      if (err) return util.error(err);
    });

    util.embed(`🔐 | Server's prefix **changed** to \`${prefix}\` by **[${msg.author}]**\n\nKeep in mind, ${nep.user} will work as well.`);

  }
}

module.exports = Setprefix;
