const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');
const maths = require('mathjs');

class Prune extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Deltes messages.`,
      longHelp: `Deletes message, or deletes from specific users.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <Amount> [ID, Tag or Mention]`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} 69`, `• ${nep.prefix}${path.basename(__filename, '.js')} 69 FortniteLover69420`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 2e3,
      aliases: ['purge'],
      locked: false,
      allowDM: false
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`:x: | Not enough arguments. Do \`${nep.prefix}help ${this.info.name}\` for more info.`);
    else if (!util.hasPermission('MANAGE_MESSAGES')) return util.hasPermission('MANAGE_MESSAGES', true);

    let amount = args[0];
    let user = args.slice(1).join(' ');

    if (!parseInt(amount) || isNaN(amount)) return util.embed(`:x: | Go **back to school**, \`${util.parseArgs(amount)}\` is not a **valid** number!`);
    else if (parseInt(amount) > 100 || parseInt(amount) < 1) return util.embed(`:x: | No, no, \`${util.parseArgs(parseInt(amount))}\` **is too ${parseInt(amount) > 100 ? 'high' : 'low'}**! ${parseInt(amount) > 100 ? 'Max' : 'Min'} is \`${parseInt(amount) > 100 ? '100!' : '1!'}\` Get good!`);
    else if (!user) return parseMessage();
    else return parseUser();

    // -------------------------------------------------------------------------

    function parseMessage() {
      amount = parseInt(maths.add(amount, 1));
      msg.channel.bulkDelete(amount).then(() => {
        util.embed(`💥 | **[${msg.author}] successfully deleted** \`${(amount)-1} messages\`!`).then((m) => m.delete({timeout: 5e3})).catch((err) => err);
      }).catch((err) => util.embed(`💢 | Could not delete **some messages** because I have **no permission**!\n\n*Deleteing in 5 seconds...*`).then((m) => m.delete({timeout: 5e3})));

    }

    // -------------------------------------------------------------------------

    function parseUser() {
      amount = parseInt( maths.add(amount, 1) );
      util.listUsers(user).then((u) => {
        msg.channel.messages.fetch({
          limit: 100,
        }).then((messages) => {
          messages = messages.filter(m => m.author.id === u.id).array().slice(0, amount);
          msg.channel.bulkDelete(messages).then(() => {
            util.embed(`💥 | **[${msg.author}] successfully deleted** \`${(amount)-1} messages\` from **[${u}]**!`).then((m) => m.delete({timeout: 5e3})).catch((err) => err);
          }).catch((err) => util.embed(`💢 | Could not delete **some messages** because I have no permission!\n\n*Deleteing in 3 seconds...*`).then((m) => m.delete({timeout: 3e3})));
        });
      }).catch((err) => util.error(err));
    }

    // -------------------------------------------------------------------------
  }
}

module.exports = Prune;
