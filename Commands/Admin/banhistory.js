const moment = require('moment');
const path = require('path');
const {
    MessageEmbed
} = require('discord.js');

module.exports.run = (client, msg, args) => {
    //set the member or user collection to the userFound variable.

    if (!client.checkPerms(client, msg, 'ADMINISTRATOR')) return client.checkPerms.sendMessage(client, msg, 'ADMINISTRATOR');
    if (!args[0]) return msg.channel.send({
        embed: new MessageEmbed()
            .setDescription(`__How to Nep 101__`)
            .addField(`Get Ban History`, `\`${client.prefix}${path.basename(__filename, '.js')} <Mention, ID, Tag, Username>\`\nThis will get the ban history of this user.`)
            .setColor(client.rColor)
    });

    client.listUsers(client, msg, args.join(' '), function(err, userFound) {
        if (err) return err;

        client.connection.query(`SELECT * FROM playerHistory WHERE userId = ${userFound.id}`, function (err, row) {
            if (row.length < 1) return msg.channel.send({
                embed: new MessageEmbed()
                    .setDescription(`✅ | \`${userFound.tag}\` is **clean** as far as I know!`)
                    .setColor(client.rColor)
            });
            else {
                var embedList = []; // List of embeds setup for pages
                row.forEach((elementy) => {
                    // User Data for each record, with date and per guild
                    embedList.push(new client.discord.MessageEmbed()
                        .setDescription(`\`${row.length} Total Ban(s)\``)
                        .addField(`📛 Tag when Banned: `, `**${elementy.tag}**`, true)
                        .addField(`🆔 User ID: `, `${elementy.userId}`, true)
                        .addField(`🖥 Guild: `, `\`${elementy.guildName}\``, true)
                        .addField(`🆔 Guild ID: `, `${elementy.guildId}`, true)
                        .addField(`📆 Date of Execution: `, `**${elementy.date}**`, true)
                        .addField(`📝 Reason:`, `\`${elementy.reason}\``, true)
                        .setThumbnail(elementy.url)
                        .setColor(client.rColor)
                    )
                })
                msg.channel.send(userFound.tag).then(async (toReact) => {
                    var positiony = 0; //Pages

                    //find the emojis for left / right / reset buttons
                    await toReact.react(`◀`);
                    await toReact.react(`🔴`);
                    await toReact.react(`▶`);

                    //Setup first page the person called will see
                    toReact.edit({
                        embed: embedList[positiony]
                    });

                    //Collector ON and OFF
                    var collector = new client.discord.ReactionCollector(toReact, m => m.users.last().id == msg.author.id, {
                        time: 60000,
                        dispose: true
                    });
                    var nowDelete = false;
                    collector.on('collect', (mm) => {
                        runLRRfunc(mm);
                    });
                    collector.on('remove', (mm) => {
                        runLRRfunc(mm);
                    });

                    //Collector per Emoji Setup
                    function runLRRfunc(mm) {
                        if (mm.emoji.name == '◀') {
                            runEdit('◀');
                        } else if (mm.emoji.name == '▶') {
                            runEdit('▶');
                        } else if (mm.emoji.name == '🔴') {
                            runEdit('🔴');
                        }
                    }

                    //Page flip functionality
                    function runEdit(vary) {
                        if (vary == '◀' && positiony > 0) {
                            positiony--;
                            toReact.edit({
                                embed: embedList[positiony]
                            });
                        }
                        if (vary == '▶' && (positiony < embedList.length)) {
                            positiony++;
                            toReact.edit({
                                embed: embedList[positiony]
                            });
                        }
                        if (vary == '🔴') {
                            positiony = 0;
                            toReact.edit({
                                embed: embedList[positiony]
                            });
                        }
                    } //End of functions
                }); //End of message embed send
            } //End of user being clean or not
        }); //End of MySQL showing List
    });

} // El fin.

module.exports.info = {
    name: path.basename(__filename, '.js'),
    category: path.dirname(__filename).split(path.sep).pop(),
    help: `See the ban history of a user on the server.`,
    longHelp: `Check if I have ever banned this user on a server.`,
    example: `${path.basename(__filename, '.js')} <Mention, ID, Username, or Tag>`,
    locked: false
}