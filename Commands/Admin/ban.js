const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');
const moment = require('moment');

class Ban extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Usual 'ban' command.`,
      longHelp: `Bans a member on the guild`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <User ID, Mention, or Tag> -reason [Reason]`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} MoistHater609`, `• ${nep.prefix}${path.basename(__filename, '.js')} MoistHater609#7212`, `• ${nep.prefix}${path.basename(__filename, '.js')} 184157133187710977`, `• ${nep.prefix}${path.basename(__filename, '.js')} MoistHater609 -reason You're fat`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: [],
      locked: false,
      allowDM: false
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`:x: | Not enough arguments. Do \`${nep.prefix}help ${this.info.name}\` for more info.`);
    else if (!util.hasPermission('BAN_MEMBERS')) return util.hasPermission('BAN_MEMBERS', true);

    let seperated = args.join(' ').toLowerCase().split(' ').indexOf('-reason');
    let user = [];
    let toBan;
    let reasonRegEx = /(?<=-reason).*$/gmi;

    if (seperated >= 0) {
      for (let i = 0; i < seperated; i++) { user.push(args[i]); }
      toBan = user.join(' ');
    }
    else toBan = args.join(' ');
    let reason = args.join(' ').toLowerCase().indexOf('-reason') > -1 ? reasonRegEx.exec(args.join(' ')) : 'No reason';

    util.listUsers(toBan).then((u) => {
      let member = msg.guild.members.get(u.id);
      let bannedEmbed = {
        color: Math.floor(Math.random() * 16777215),
        author: {
          name: moment().format(`MMMM Do, YYYY`),
          icon_url: u.avatarURL({size: 2048})
        },
        fields: [
          {
            name: `<:ThinkBan:423651900043427840> | Ban`,
            value: `**[${member}]** \`by\` **[${msg.author}]**`
          },
          {
            name: `📋 | Reason`,
            value: `\`${util.parseArgs(reason)}\``
          }
        ]
      };

      member.ban(reason !== undefined ? util.parseArgs(reason) : '').then((banned) => {
        msg.channel.send({embed: bannedEmbed})
        u.send({embed: bannedEmbed}).catch((err) => err);
      }).catch((err) => util.error(`Error when banning user:\n\n${err}`));

    }).catch((err) => util.error(err));

  }
}

module.exports = Ban;
