const Command = require('/home/moist/Bots/NepV5/Classes/Command.js');
const path = require('path');
const curl = require('curl');
const moment = require('moment');

class Massban extends Command {
  constructor(nep) {
    super(nep, {
      name: path.basename(__filename, '.js'),
      help: `Mass bans mutliple users.`,
      longHelp: `Bans multiple users from the server.`,
      usage: [`• ${nep.prefix}${path.basename(__filename, '.js')} <User1, User2, User3, etc.> -reason [reason]`],
      examples: [`• ${nep.prefix}${path.basename(__filename, '.js')} @MoistHater609, 135148181683044352`, `• ${nep.prefix}${path.basename(__filename, '.js')} @MoistHater609, 135148181683044352 -reason You suck xd`],
      category: path.dirname(__filename).split(path.sep).pop(),
      cooldown: 1e3,
      aliases: ['mban'],
      locked: false,
      allowDM: false
    });
  }

  // ---------------------------------------------------------------------------

  run(msg, util, args, nep) {
    if (!args[0]) return util.embed(`:x: | Not enough arguments. Do \`${nep.prefix}help ${this.info.name}\` for more info.`);
    else if (!util.hasPermission('ADMINISTRATOR')) return util.hasPermission('ADMINISTRATOR', true);

    let seperated = args.join(' ').toLowerCase().split(' ').indexOf('-reason');
    let user = [];
    let toBan;
    let reasonRegEx = /(?<=-reason).*$/gmi;

    if (seperated >= 0) {
      for (let i = 0; i < seperated; i++) { user.push(args[i]); }
      toBan = user.join(' ');
    }
    else toBan = args.join(' ');
    let reason = args.join(' ').toLowerCase().indexOf('-reason') > -1 ? reasonRegEx.exec(args.join(' ')) : 'No reason';

    let mentions = msg.mentions.members;
    let ids = toBan.replace(/,/g, ' ').replace(/\s+/g, ' ').split(' ');

    let userList = [];

    for (let i = 0; i < ids.length; i++) {
      mentions.map((m) => {
        let member = msg.guild.members.get(ids[i]);

        if (member) return userList.push(member);
        else if (m) return userList.push(m);
      });
    }
    let usersToBan = userList.filter((elem, pos) => {
      return userList.indexOf(elem) == pos;
    });
    let bannedEmbed;

    if (usersToBan.length <= 0 || userList.length <= 0) return util.embed(`:x: | No users found **matching your query**! Only **mentions and IDs** work!`);

    new Promise((resolve, reject) => {
      usersToBan.forEach((member) => {
        member.ban(reason == undefined ? '' : util.parseArgs(reason)).then((banned) => {
          bannedEmbed = {
            color: Math.floor(Math.random() * 16777215),
            author: {
              name: moment().format(`MMMM Do, YYYY`),
              icon_url: usersToKick[0].user.avatarURL({size: 2048})
            },
            fields: [
              {
                name: `<:ThinkBan:423651900043427840> | Ban(s)`,
                value: `**[${usersToBan.length > 3 ? `${usersToBan[0]} | ${usersToBan[1]} | ${usersToBan[2]}+` : usersToBan.join(' | ')}]** \`by\` **[${msg.author}]**`
              },
              {
                name: `📋 | Reason`,
                value: `\`${util.parseArgs(reason)}\``
              }
            ]
          };
          banned.send({embed: bannedEmbed}).catch((err) => err);
          resolve();
        }).catch((err) => reject(err));
      });
    }).then(() => {
      msg.channel.send({embed: bannedEmbed}).catch((err) => util.error(err));
    }).catch((err) => util.error(`Error when banning user(s):\n\n${err}`));

  }
}

module.exports = Massban;
