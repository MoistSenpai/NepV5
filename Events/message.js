let thisPrefix;

module.exports = class {
  constructor(nep) {
    this.nep = nep;
  }

  // ---------------------------------------------------------------------------

  run(msg) {
    let nep = this.nep; // Make nep into nep variable

    nep.rColor = Math.floor(Math.random() * 16777215).toString(16); // Random color generator
    nep.util = new(require(`../Classes/Utils.js`))(nep, msg); // Nep Utils class

    if (msg.author.bot) return;

    nep.util.selectAll(`servers`, 'guild').then((row) => {
      nep.prefix = msg.guild ? row.length > 0 ? row[0].prefix : nep.prefix : '--';
  		let prefixes = [nep.prefix, '—',`<@${nep.user.id}>`, `<@!${nep.user.id}>`]

  		for (thisPrefix of prefixes) {
  			if (msg.content.startsWith(thisPrefix)) nep.prefix = thisPrefix;
  		}
      // return msg.channel.send(nep.prefix);
      if (msg.guild) nep.connection.query(`INSERT INTO servers (guildId, prefix) VALUES (?, ?) ON DUPLICATE KEY UPDATE guildId = ${msg.guild.id}`, [msg.guild.id, nep.prefix]);

      //Remove Discord link
      let inviteReg = /(https?:\/\/)?(www\.)?(discord\.(gg|io|me|li)|discordapp\.com\/invite)\/.+[a-z]/g;

      if (inviteReg.exec(msg.content) && msg.channel.id !== '525398220684263433' && msg.guild.id == `525139353610223627`) {
        msg.delete().then(() => {
          msg.channel.send({
            embed: new nep.discord.MessageEmbed()
              .setDescription(`😤 | Hey **[${msg.author}]**, you can only posts **invite links** in <#525398220684263433>!\n\n*This message will delete in 10 seconds*`)
              .setColor(nep.rColor)
          }).then((m) => m.delete({timeout: 10e3}));
        });
      }

      // Reaction stuff
      nep.util.selectAll('reactions', 'guild').then((row) => {
        let obj = {
  			  message: [],
  			  guild: [],
  			  reaction: [],
  			  include: []
  			}

  			for (let i = 0; i < row.length; i++) {
  			  if (row[i]['include'] == true && msg.content.toLowerCase().indexOf(row[i]['message'].toLowerCase()) >= 0 && msg.guild.id == row[i]['guildId']) {
  			    obj.message.push(msg.content);
  			    obj.guild.push(msg.guild.id);
  			    obj.reaction.push(row[i]['reaction']);
  			    obj.include.push(row[i]['include']);
  			  } else if (row[i]['include'] == false && msg.content.toLowerCase() == row[i]['message'].toLowerCase() && msg.guild.id == row[i]['guildId']) {
  			    obj.message.push(msg.content);
  			    obj.guild.push(msg.guild.id);
  			    obj.reaction.push(row[i]['reaction']);
  			    obj.include.push(row[i]['include']);
  			  }
    			}

  			let rand = (thing) => {
  			  return Math.floor(Math.random() * thing.length);
  			}

  			if (obj.include[rand(obj.include)] == false && obj.message.indexOf(msg.content) == 0 && obj.guild.indexOf(msg.guild.id) >= 0) {
  			  if (obj.reaction.indexOf(`@everyone`) >= 0) {
  			    msg.channel.send(obj.reaction[rand(obj.reaction)].toString().replace(/@everyone/i, '@Everyone'));
  			  } else {
  			    msg.channel.send(obj.reaction[rand(obj.reaction)]);
  			  }
  			} else if (obj.include[rand(obj.include)] == true && obj.message.indexOf(msg.content) >= 0 && obj.guild.indexOf(msg.guild.id) >= 0) {
  			  if (obj.reaction.indexOf(`@everyone`) >= 0) {
  			    msg.channel.send(obj.reaction[rand(obj.reaction)].toString().replace(/@everyone/i, '@Everyone'));
  			  } else {
  			    msg.channel.send(obj.reaction[rand(obj.reaction)]);
  			  }
  			}

        if (msg.author.bot || !msg.content.startsWith(nep.prefix)) return; // Ignore bots and no prefix

        // Ignore stuff
        nep.util.selectAll(`ignoring`, `guild`).then((row2) => {

          let args = msg.content.slice(nep.prefix.length).trim().split(/ +/g);
          let command = args.shift();
          // let args = msg.content.split(/\s+/g);
          // let command = args.shift().slice(nep.prefix.length).toLowerCase();
          let cmd = nep.commands.get(command) || nep.commands.get(nep.aliases.get(command));

          // Hold all ignores for guild
          let ignores = {
            channel: [],
            role: [],
            user: []
          }

          // Push ingores for guild into object
          for (let i = 0; i < row2.length; i++) {
            // if (row2[i].channelId == null || row2[i].roleId == null || row2[i].channelId == null) { continue; }
            ignores.channel.push(row2[i].channelId); // Channel
            ignores.role.push(row2[i].roleId); // Role
            ignores.user.push(row2[i].userId); // User
          }

          if (!cmd) return; // If no command return

          let isOwner = msg.author.id == '184157133187710977' || msg.author.id == '251091302303662080';
          let isAdminCmd = cmd.info.category.toLowerCase() == 'admin';

          if (ignores.user.indexOf(msg.author.id) >= 0 && !isOwner && !isAdminCmd) return; // Check if user matches ingore
          else if (ignores.channel.indexOf(msg.channel.id) >= 0 && !isOwner && !isAdminCmd) return; // Check if channel matches ingore
          else if (ignores.channel.indexOf(msg.channel.parentID) >= 0 && !isOwner && !isAdminCmd) return; // Check if category matches ingore
          else if (msg.guild && ignores.role.indexOf(msg.member.roles.map((r) => r.id)) >= 0 && !isOwner && !isAdminCmd) return; // Check if role matches ingore
          else if (cmd.cooldown.has(msg.author.id)) { // Handle command cooldown
            if (cmd.sentCooldownMessage.has(msg.author.id)) return;
            else return msg.channel.send({
              embed: new nep.discord.MessageEmbed()
                .setDescription(`⏲ | *Please wait* \`${nep.util.msParser(cmd.config.cooldown)}\` *until using this command again!*`)
                .setColor(nep.rColor)
                .setFooter(msg.author.tag, msg.author.displayAvatarURL())
            }).then(() => cmd.sentCooldownMessage.add(msg.author.id));
          }

          // Command handler
          try {
            cmd.setMessage(msg);

            if (!cmd.config.allowDM && !msg.guild) return nep.util.embed(`:x: | This command cannot be used in a DM!`);
            if (cmd.config.cooldown > 0) cmd.startCooldown(msg.author.id);
            if (cmd.config.locked && msg.author.id !== `184157133187710977` && msg.author.id !== `251091302303662080`) return nep.util.embed(`🔒 | \`${command}\` has been **locked to the public**! Try again later!`);
            if (cmd.info.category == 'Moisty' && msg.author.id !== `184157133187710977` && msg.author.id !== `251091302303662080`) return msg.channel.send(`Fuck off`);
            cmd.run(msg, nep.util, args, nep);
          } catch (err) {
            nep.util.error(`${err.stack}`);
          }

        }).catch((err) => nep.util.error(err.stack)); // Ignore end
      }).catch((err) => nep.util.error(err.stack)); // Reaction end
    }).catch((err) => nep.util.error(err.message)); // Server Prefix End
  } // Method End
} // Class End
