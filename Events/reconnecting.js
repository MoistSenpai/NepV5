const moment = require('moment-timezone');

module.exports = class {
  constructor(nep) {
    this.nep = nep;
  }

  // ---------------------------------------------------------------------------

  run(shard) {
    let nep = this.nep;

    nep.util = new(require(`../Classes/Utils.js`))(nep); // Nep Utils class
    nep.util.log(`Reconnecting Event`, `Shard ${shard} of bot is reconnecting to websocket`);

  }
}
