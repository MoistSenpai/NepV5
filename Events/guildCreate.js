const moment = require('moment-timezone');
const Long = require('long');

module.exports = class {
  constructor(nep) {
    this.nep = nep;
  }

  // ---------------------------------------------------------------------------

  run(guild) {
    let nep = this.nep;

    let toMsgChannel = nep.channels.get(`378364387934601217`);
    let newGuildEmbed = new nep.discord.MessageEmbed()
        .setDescription(`New Guild Joined: **[\`${guild.owner.user.tag}\`]**`)
        .addField(`Name`, `**${guild.name}** (${guild.id})`)
        .addField(`Members`, `**${guild.memberCount}** members`)
        .setFooter(moment().tz('America/New_York').format(`MM/DD/YYYY | hh:mm A`), guild.iconURL())
        .setColor(`#00FFFF`)

    toMsgChannel.send({
        embed: newGuildEmbed
    });

    function getDefaultChannel(g) {
      if (g.channels.has(g.id)) return g.channels.get(g.id)
      if (g.channels.some((c) => c.name == 'general')) return g.channels.some((c) => c.name == 'general');
      return g.channels.filter(c => c.type === 'text' &&
       c.permissionsFor(nep.user).has('SEND_MESSAGES'))
       .sort((a, b) => a.position - b.position ||
       Long.fromString(a.id).sub(Long.fromString(b.id)).toNumber()).first();
    }


    getDefaultChannel(guild).send({
      embed: new nep.discord.MessageEmbed()
        .setDescription(`Hi hi! I'm the Neptune the neppiest of all Neps around! Here's some info about me:\nMy creator is **${nep.users.get('184157133187710977').tag}** I was built using **Javascript with NodeJS Framework** and my libary is **Discord.JS**.\n To see all my commands and how to use them, do '${nep.user} help'!`)
        .addField(`Nep Nep Server`, `• **[Support Server](https://discord.gg/R9ykDC3)**`)
        .setColor(`#00FFFF`)
        .setThumbnail(nep.user.avatarURL({size: 2048}))
    });
  }
}
