const moment = require('moment');

let thisPrefix;

module.exports = (client, oMsg, nMsg) => {
	if (nMsg.author.bot || !nMsg.guild) return;

	// Servers Prefixes
	client.connection.query(`SELECT * FROM servers WHERE guildId = ${nMsg.guild.id}`, function (err, row) {
		if (err) return client.log(`MySQL Error`, err, `Server Query`);

		client.prefix = row.length > 0 ? row[0].prefix : client.prefix;
		let prefixes = [client.prefix, `<@${client.user.id}> `, `<@!${client.user.id}> `, `—`];

		for (thisPrefix of prefixes) {
			if (nMsg.content.toLowerCase().startsWith(thisPrefix)) client.prefix = thisPrefix;
		}


		client.connection.query(`INSERT INTO servers (guildId, prefix)
      VALUES (?, ?) ON DUPLICATE KEY UPDATE guildId = ${nMsg.guild.id}`, [nMsg.guild.id, client.prefix]);


			if ((nMsg.content.toLowerCase().indexOf(client.prefix) !== 0)) return;

			const args = nMsg.content.slice(client.prefix.length).trim().split(/ +/g);
			const command = args.shift().toLowerCase();

			client.connection.query(`SELECT * FROM ignoring WHERE guildId = ${nMsg.guild.id}`, function (err, row) {
				if (err) return client.log(`MySQL Error`, err, `Ignore Query`);

				if (row.length > 0) {
					for (let i = 0; i < row.length; i++) {
						if (nMsg.author.id == row[i].userId) return;
						if (command in client.commands) {
							if (row[i].guildId == nMsg.guild.id && row[i].channelId == nMsg.channel.id && client.commands[command].info.category !== 'Admin' && client.commands[command].info.category !== 'Owner') return;
						} else if (row[i].guildId == nMsg.guild.id && row[i].channelId == nMsg.channel.id) return;
					}
				}

				// Handle Commands and Stuff

				try {
          if (moment(oMsg.createdTimestamp).add(5, 'seconds').isBefore(nMsg.editedAt)) return;
					if (command in client.commands && client.commands[command].info.locked && nMsg.author.id !== '184157133187710977' && nMsg.author.id !== `251091302303662080`) return nMsg.channel.send({
						embed: new client.discord.MessageEmbed()
							.setDescription(`:x: | Whoops, this command is **under maintenance **! Check back later!`)
							.setColor(client.rColor)
					});
					if (command in client.commands && client.commands[command].info.category == 'Music') return;
          if (command in client.commands && client.commands[command].info.category == 'Owner' && nMsg.author.id !== '184157133187710977' && nMsg.author.id !== `251091302303662080`) return nMsg.channel.send(`Fuck off`);
					if (command in client.commands) client.commands[command].run(client, nMsg, args);
				} catch (error) {
					nMsg.channel.send({
						embed: new client.discord.MessageEmbed()
							.setDescription(`:x: | Oh sheet, an error occured while executing \`${command}\`!\n\`\`\`css\n${error.stack}\n\`\`\``)
							.setColor(client.rColor)
					});
				}
			}); // Ignore
	}); // Servers Prefixes
}
