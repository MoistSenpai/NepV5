const moment = require('moment-timezone');

module.exports = class {
  constructor(nep) {
    this.nep = nep;
  }

  // ---------------------------------------------------------------------------

  run(event, shard) {
    let nep = this.nep;

    nep.util = new(require(`../Classes/Utils.js`))(nep); // Nep Utils class
    nep.util.log(`Disconnected Event`, `Shard ${shard} of bot has been disconnected and will not reconnect.`);

  }
}
