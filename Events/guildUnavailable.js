module.exports = class {

	constructor(nep) {
		this.nep = nep;
	}

	run(guild) {
		let nep = this.nep;
		
		nep.util = new(require(`../Classes/Utils.js`))(nep); // Nep Utils class
		nep.util.log(`Guild Unavailable Event`, `Guild ${guild.name} is unavailable, likely due to server outage`);
	}

}