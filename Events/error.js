module.exports = class {

	constructor(nep) {
		this.nep = nep;
	}

	run(err) {
		let nep = this.nep;
		
		nep.util = new(require(`../Classes/Utils.js`))(nep); // Nep Utils class
		nep.util.log(`Error Event`, JSON.stringify(err));
	}

}