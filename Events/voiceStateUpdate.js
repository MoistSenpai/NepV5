module.exports = class {

	constructor(nep) {
		this.nep = nep;
	}

	run(oldMember, newMember) {
		let nep = this.nep;
		nep.util = new(require(`../Classes/Utils.js`))(nep); // Nep Utils class

		let voiceCon = oldMember.guild.voiceConnection;

		if (voiceCon && voiceCon.channel.members.has(nep.user.id) && voiceCon.channel.members.size == 1) {
			let queue = nep.util.getQueue(oldMember.guild.id);

			queue.splice(0, queue.length);
			voiceCon.channel.leave();
		}


	}

}
