const moment = require('moment-timezone');

module.exports = class {
  constructor(nep) {
    this.nep = nep;
  }

  // ---------------------------------------------------------------------------

  run(guild) {
    let nep = this.nep;

    let toMsgChannel = nep.channels.get(`378364387934601217`);
    let newGuildEmbed = new nep.discord.MessageEmbed()
        .setDescription(`Guild Remove: **[\`${guild.owner.user.tag}\`]**`)
        .addField(`Name`, `**${guild.name}** (${guild.id})`)
        .addField(`Members`, `**${guild.memberCount}** members`)
        .setFooter(moment().tz('America/New_York').format(`MM/DD/YYYY | hh:mm A`), guild.iconURL())
        .setColor(`#FF0000`);

    toMsgChannel.send({
        embed: newGuildEmbed
    });

  }
}
