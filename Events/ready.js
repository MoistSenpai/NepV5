module.exports = class {

  constructor(nep) {
    this.nep = nep;
  }

  run() {
    let nep = this.nep;
    nep.utils = new (require(`../Classes/Utils.js`))(nep); // Nep utilss class

    nep.utils.log(`Ready`, `Client logged in and ready`, `${nep.user.tag}`); // Client logged in and is ready


    //Mysql safety net by Emp
    nep.connection.connect(function(err) {
      if (err) {
        console.error('error connecting: ' + err.stack);
        return;
      }

      // console.log(`[MySQL] Mysql safely connected as id ${nep.connection.threadId}`.yellow)
      // console.log(` [Emp was here. Nep couldn't connect to Mysql server "ERRCONNECTION Refused" error. I added this clip of code in ready.js file so if it works itll connect, if it doesn't connect it will stop bot (instead of spamming an error on every message on every 1 thousand servers.)"]`.green);
    });
    //End of Mysql safety net by Emp


    nep.connection.query(`SELECT * FROM playingStatus`, function(err, row) { // Get playing status data from table
      if (err) return nep.utils.log(`MySQL Error`, err.message); // Handle error
      else if (row.length <= 0) return nep.utils.log(`Playing Status`, `None in Database`); // If no status in database

       let presence = nep.user.presence.status;

      nep.user.setActivity(row[0].status, {type: row[0].type}); // Set status
      nep.utils.log(`Status`, `${row[0].type}: ${row[0].status}`, `${presence[0].toUpperCase()}${presence.split(presence[0]).join('')}`); // Log status

    });

  }

}
