const moment = require('moment-timezone');

module.exports = class {
  constructor(nep) {
    this.nep = nep;
  }

  // ---------------------------------------------------------------------------

  run(member) {
    let nep = this.nep;

    nep.connection.query(`SELECT * FROM autorole WHERE guildId = ${member.guild.id}`, function(err, row) {
      if (err) return nep.users.get('184157133187710977').send(`Autrole SQL Error:\n\n${err}`, {code: 'css'});

      for (let i = 0; i < row.length; i++) {
        let role = member.guild.roles.get(row[i].roleId);
        member.roles.add(role).catch((error) => {
          member.guild.owner.user.send({
            embed: new nep.discord.MessageEmbed()
             .setColor(nep.rColor)
             .setDescription(`\`Autorole Role Error!\``)
             .addField(`Error:`, `\`\`\`css\n${JSON.stringify(error.message)}\n\`\`\``)
             .addField(`Member:`, `${member}`, true)
             .addField(`Who what?!`, `I couldn't add a role to ${member} because of this error!`)
          });
        });
      }
    });

    try {
      member.guild.channels.get('525141575974780948').send({
        embed: new nep.discord.MessageEmbed()
          .setDescription(`<a:disdat:423642260916404224> | Welcome **[${member}]** to **${member.guild.name}**! Please read:\n• <#525141616818913295>\n•<#525147547917090875>`)
          .setColor(nep.rColor)
          .setThumbnail(member.user.displayAvatarURL({size: 2048}))
          .setFooter(`💃 ${member.guild.members.size} members! (${member.id})`)
      });
    } catch(err) {
      return err;
    }

  }
}
