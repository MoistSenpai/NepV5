A Discord bot named Neptune made by Moist#9999
=======
Installing Packages:

'npm i --save'

MySQL Stuff:

Look at 'MySQLSetup.txt'

Config:

Make a copy of 'config.json.sample' and remove the '.sample', now you should just have 'config.json'.
Open it, and make all the changes, be sure to remove the '// ...'

Running:

Requires NodeJS, open terminal to the directory and type 'node index.js'.

Misc.:

I will not be giving out any APIs keys, such as Wolke or my own personal API.
You'll have to get around that yourself. It's open source for a reason ;^)
