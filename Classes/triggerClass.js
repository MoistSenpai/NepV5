const fs = require('fs');
const gm = require('gm');
const dir = `/var/www/pics`;
const https = require('https');
const http = require('http');

class TriggerClass {
  constructor(msg, util, args, nep, cmd) {
    this.msg = msg;
    this.util = util;
    this.args = args;
    this.nep = nep;
    this.cmd = cmd;
  }

  // -----------------------------------------

  user(person) {
    let msg = this.msg;
    let util = this.util;
    let args = this.args;
    let nep = this.nep;
    let cmd = this.cmd;
    let t = this;

    if (!person || person === '' || person === undefined) return mention();
    else return self();

    // -----------------------------------------

    function mention() {
      nep.util.listUsers(args.join(' ')).then((user) => {
        return t.pic(user.displayAvatarURL({size: 2048}));
      }).catch((err) => util.error(err));
    }

    // -----------------------------------------

    function self() {
      let user = person;
      return t.pic(user.displayAvatarURL({size: 2048}));
    }

    // -----------------------------------------
  }

  // -----------------------------------------

  url(link) {
    let msg = this.msg;
    let util = this.util;
    let args = this.args;
    let nep = this.nep;
    let cmd = this.cmd;
    let t = this;

    msg.delete({timeout: 3e3}).catch((err) => err);
    return t.pic(link);
  }

  // -----------------------------------------

  pic(url) {
    let msg = this.msg;
    let util = this.util;
    let args = this.args;
    let nep = this.nep;
    let cmd = this.cmd;

    let id = Math.random().toString(36).substr(2, 5);
    let thing = url.toLowerCase().startsWith('https') ? https : http;

    thing.get(url.replace(/\.webp/gi, '.png'), async (resp) => {
      // if (err) return util.error(`Error when grabbing URL:\n\n${err}`);
      await resp.pipe(fs.createWriteStream(`${nep.dir}/Temp/${id}.png`));

      util.send(`*Triggering...*`).then(async (m) => {

        new Promise((resolve) => {
          for (let i = 1; i < 3; i++) {
            let x = 0;
            let y = 0;
            let x2 = 0;
            let y2= 0;

            switch (i) {
              case 1:
                x = 10;
                y = 10;
                x2 = -10;
                y2 = 580;
              break;
              case 2:
                x = 10;
                y = 0;
                x2 = -2;
                y2 = 572;
              break;
            }

            gm(`${nep.dir}/Temp/base.png`)
            .resize(800, 800)
            .draw(`image Over` + ` ${x}, ${y},` + `800, 647` + `${nep.dir}/Temp/${id}.png`)
					  .draw(`image Over` + ` ${x}, ${y},` + `800, 650` + `${nep.dir}/Temp/redTint.png`)
					  .draw(`image Over` + ` ${x2}, ${y2},` + `800, 647` + `${nep.dir}/Temp/banner.png`)
					  .write(`${nep.dir}/Temp/${id}-${i}.png`, function(err) {
						  if (err) return util.error(`Part ${i} Making gif:\n\n${err}`);
						  resolve();
					  });

          }
        }).then(() => {

          gm(`${nep.dir}/Temp/base.png`)
          .resize(800, 800)
          .in(`${nep.dir}/Temp/${id}-1.png`)
          .in(`${nep.dir}/Temp/${id}-2.png`)
          .delay(3)
          .write(`${nep.dir}/Temp/${id}-6.gif`, async function(err) {
            if (err) return util.error(`Final Making gif:\n\n${err}`);

            let file = new nep.discord.MessageAttachment(`${nep.dir}/Temp/${id}-6.gif`);
            await msg.channel.send(`*T R I G G E R D* <a:FuckingTriggerd:426191845287002152>`, file).catch((err) => m.edit(`Error sending image, fix your stuff!\n\n\`\`\`css\n${err}\`\`\``));
            await fs.unlinkSync(`${nep.dir}/Temp/${id}.png`);
            await fs.unlinkSync(`${nep.dir}/Temp/${id}-1.png`);
            await fs.unlinkSync(`${nep.dir}/Temp/${id}-2.png`);
            await fs.unlinkSync(`${nep.dir}/Temp/${id}-6.gif`);
            await m.delete({timeout: 10});
            
          });

        }).catch((err) => util.error(err));
      }).catch((err) => util.error(`Error when making GIF:\n\n${err}`));


    });

  }

  // -----------------------------------------
}

module.exports = TriggerClass;
