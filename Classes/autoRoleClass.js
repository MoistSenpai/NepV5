class AutoRoleClass {
  constructor(msg, util, args, nep, flag, postFlag, cmd) {
    this.msg = msg;
    this.util = util;
    this.args = args;
    this.nep = nep;
    this.flag = flag;
    this.postFlag = postFlag;
    this.cmd = cmd;
  }
  // ---------------------------------------------------------------------------

  add() {
    let msg = this.msg;
    let util = this.util;
    let args = this.args;
    let nep = this.nep;
    let flag = this.flag;
    let postFlag = this.postFlag;
    let cmd = this.cmd;

    util.selectAll('autorole', 'guild').then((row) => {
      let alreadyIn = [];

      util.listRoles(postFlag).then((r) => {
        let toSend = `📌 | Added role **[${r}]** to the autorole list!`;

        if (row.length <= 0) {
          nep.connection.query(`INSERT INTO autorole (guildId, roleId) VALUES (?, ?) ON DUPLICATE KEY UPDATE roleId = ${r.id}`, [msg.guild.id, r.id]);
          return util.embed(toSend);
        }

        for (let i = 0; i < row.length; i++) { alreadyIn.push(row[i].roleId); };

        if (alreadyIn.indexOf(r.id) >= 0) return util.embed(`:x: | **[${r}]** is **already in** autorole! Use \`${nep.prefix}${cmd.info.name} -list\` to check.`);

        nep.connection.query(`INSERT INTO autorole (guildId, roleId) VALUES (?, ?) ON DUPLICATE KEY UPDATE roleId = ${r.id}`, [msg.guild.id, r.id]);
        return util.embed(toSend);

      }).catch((err) => util.error(err));
    }).catch((err) => util.error(err));

  }

  // ---------------------------------------------------------------------------

  remove() {
    let msg = this.msg;
    let util = this.util;
    let args = this.args;
    let nep = this.nep;
    let flag = this.flag;
    let postFlag = this.postFlag;
    let cmd = this.cmd;

    util.selectAll('autorole', 'guild').then((row) => {
      let alreadyIn = [];

      util.listRoles(postFlag).then((r) => {
        let toSend = `✂ | The role **[${r}]** has **been removed** from the autorole!`;

        if (row.length <= 0) return util.embed(`:x: | **[${r}]** isn't **even in the autorole**! What do you want from me?`);

        for (let i = 0; i < row.length; i++) { alreadyIn.push(row[i].roleId); };

        if (alreadyIn.indexOf(r.id) < 0) return util.embed(`:x: | **[${r}]** **isn't even in the autorole**! What do you want from me?`);

        nep.connection.query(`DELETE FROM autorole WHERE roleId = ${r.id} AND guildId = ${msg.guild.id}`);
        return util.embed(toSend);

      });

    }).catch((err) => util.error(err));

  }

  // ---------------------------------------------------------------------------

  list() {
    let msg = this.msg;
    let util = this.util;
    let args = this.args;
    let nep = this.nep;
    let flag = this.flag;
    let postFlag = this.postFlag;
    let cmd = this.cmd;

    util.selectAll('autorole', 'guild').then((row) => {
      let roles = [];

      if (row.length <= 0) return util.embed(`:x: | *Can't list something if there's nothing to list*! <:WeSmart:421536576707887114>`);

      for (let i = 0; i < row.length; i++) { roles.push(msg.guild.roles.get(row[i].roleId)); };

      return util.sendAutoRoleList(roles);
    }).catch((err) => util.error(err));

  }

  // ---------------------------------------------------------------------------
}

module.exports = AutoRoleClass;
