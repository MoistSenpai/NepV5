class ReactionClass {
  constructor(msg, util, args, nep, message, reaction, cmd, sep) {
    this.msg = msg;
    this.util = util;
    this.args = args;
    this.nep = nep;
    this.message = message;
    this.reaction = reaction;
    this.cmd = cmd;
    this.sep = sep;
  }

  // ---------------------------------------------------------------------------

  add() {
    let msg = this.msg;
    let util = this.util;
    let args = this.args;
    let nep = this.nep;
    let message = this.message;
    let reaction = this.reaction;
    let cmd = this.cmd;
    let sep = this.sep;

    let id = this.genId(5);
    let toSend = `✅ | Ok, I will now **react to** \`${message}\` (**${id}**)!`;

    if (sep <= 0) return util.embed(`:x: | You must have an \`=>\` **seperating** your message and reaction.\n\nEx. \`${nep.prefix}${cmd.info.name} -add Hello => Shut up\``);
    else if (!message) return util.embed(`:x: | Your message cannot be empty!`);
    else if (!reaction) return util.embed(`:x: | Your reaction cannot be empty!`);

    // util.embed(`${toSend}`);

    nep.connection.query(`INSERT INTO reactions (guildId, message, reaction, id, include) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE id = \'${id}\'`, [msg.guild.id, message, reaction, id, false], function(err) {
      if (err) return util.error(err);
      return util.embed(toSend);
    });
  }

  // ---------------------------------------------------------------------------

  include() {
    let msg = this.msg;
    let util = this.util;
    let args = this.args;
    let nep = this.nep;
    let message = this.message;
    let reaction = this.reaction;
    let cmd = this.cmd;
    let sep = this.sep;

    let id = this.genId(5);
    let toSend = `✅ | Ok, I will now **react to anything containing** \`${message}\` (**${id}**)!`;

    if (sep <= 0) return util.embed(`:x: | You must have an \`=>\` **seperating** your message and reaction.\n\nEx. \`${nep.prefix}${cmd.info.name} -add Hello => Shut up\``);
    else if (!message) return util.embed(`:x: | Your message cannot be empty!`);
    else if (!reaction) return util.embed(`:x: | Your reaction cannot be empty!`);

    nep.connection.query(`INSERT INTO reactions (guildId, message, reaction, id, include) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE id = \'${id}\'`, [msg.guild.id, message, reaction, id, true], function(err) {
      if (err) return util.error(err);
      return util.embed(toSend);
    });
  }

  // ---------------------------------------------------------------------------

  remove() {
    let msg = this.msg;
    let util = this.util;
    let args = this.args;
    let nep = this.nep;
    let message = this.message;
    let reaction = this.reaction;
    let cmd = this.cmd;
    let sep = this.sep;

    let ids = [];
    let toDelte = args.slice(1).toString();

    if (!toDelte || toDelte == '' || toDelte === undefined) return util.embed(`:x: | You must provide an **ID to delete**. \`${nep.prefix}${cmd.info.name} -list\` to find ID(s).`);

    util.selectAll('reactions', 'guild').then((row) => {
      if (row.length <= 0) return util.send(`:x: | This server has no reactions to delete 🤷!`);
      for (let i = 0; i < row.length; i++) { ids.push(row[i].id); }

      if (ids.indexOf(toDelte) < 0) return util.embed(`:x: | Cannot **find any ID(s)** matching \`${util.parseArgs(toDelte)}\`, try again!`);

      nep.connection.query(`DELETE FROM reactions WHERE id = \"${toDelte}\" AND guildId = ${msg.guild.id}`, function(err) {
        if (err) return util.error(err);
        return util.embed(`🙅 | **[${msg.author}]** removed reaction with ID \`${toDelte}\`!`);
      });

    }).catch((err) => util.error(err));

  }

  // ---------------------------------------------------------------------------

  purge() {
    let msg = this.msg;
    let util = this.util;
    let args = this.args;
    let nep = this.nep;
    let message = this.message;
    let reaction = this.reaction;
    let cmd = this.cmd;
    let sep = this.sep;

    if (!util.hasPermission('ADMINISTRATOR')) return util.hasPermission('ADMINISTRATOR', true);

    let suf = args.slice(1).join(' ').toLowerCase();

    if (!suf[0] || !suf.startsWith('-y') && !!suf.startsWith('-yes') && !suf.startsWith('y') && !suf.startsWith('yes')) return util.embed(`🔴 | This will **DELETE ALL** reactions, try again with: \`${nep.prefix}${cmd.info.name} -purge -y\` to confirm.`);

    nep.connection.query(`DELETE FROM reactions WHERE guildId = ${msg.guild.id}`, function(err) {
      if (err) return util.error(err);
      return util.embed(`🐲 | **[${msg.author}]** deleted **all reactions** on the server.`);
    });

  }

  // ---------------------------------------------------------------------------

  list() {
    let msg = this.msg;
    let util = this.util;
    let args = this.args;
    let nep = this.nep;
    let message = this.message;
    let reaction = this.reaction;
    let cmd = this.cmd;
    let sep = this.sep;

    let list = [];

    util.selectAll('reactions', 'guild').then((row) => {
      if (row.length <= 0) return util.embed(`:x: | *Can't list something if there's nothing to list*! <:WeSmart:421536576707887114>`);

      for (let i = 0; i < row.length; i++) { list.push(`\`${row[i].id}\`: **${util.parseArgs(row[i].message)}** => **${util.parseArgs(row[i].reaction)}**`); }

      return util.sendReactionList(list);

    }).catch((err) => util.error(err));

  }

  // ---------------------------------------------------------------------------

  genId(length) {
    return '+' + Math.random().toString(36).substr(2, length);
  }

  // ---------------------------------------------------------------------------
}

module.exports = ReactionClass;
