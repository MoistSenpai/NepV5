class Level {
    constructor(client, author, row) {
        this.client = client;
        this.author = author;
        this.row = row;
    }
    insertLevel() { // INSERT into level table default values
        this.client.connection.query(`INSERT INTO level (userId, xp, level, pudding, timeUsed, background) VALUES (?, ?, ?, ?, ?, ?) 
        ON DUPLICATE KEY UPDATE userId = ${this.author.id}`, [
            this.author.id, // UserId
            this.xp, // Random XP
            0, // Pudding
            null, // Time used
            `/home/moist/Bots/NepV4/TempFiles/Level/Backgrounds/default.png` // Default background
        ]);
    }
    updateLevel(row, newValue, quotes) {
        if (quotes) return this.client.connection.query(`UPDATE level SET ${row} = \"${newValue}\" WHERE userId = ${this.author.id}`);
        else return this.client.connection.query(`UPDATE level SET ${row} = ${newValue} WHERE userId = ${this.author.id}`);
    }
    get xp() {
        return Math.floor(Math.random() * 15);
    }
}

module.exports = Level;