const curl = require('curl');

class QueueClass {
  constructor(msg, util, args, nep) {
    this.msg = msg || undefined;
    this.util = util || undefined;
    this.args = args || undefined;
    this.nep = nep || undefined;
  }

  // ---------------------------------------------------------------------------

  showQueue(queue) { // Show the guild's current queue
    if (queue.length === 0) return this.util.embed(`*You can't list anything if there's nothing to list <:WeSmart:421536576707887114>*`);

    // Map the queue into readable format then send
    let voiceConnection = this.msg.guild.voiceConnection;
    let queueStatus = !voiceConnection || voiceConnection.paused ? 'stopped' : 'playing';

    return this.util.showQueuePages(queue, queueStatus);
  }

  // ---------------------------------------------------------------------------

  add(queue) { // Add items to the queue
    let nep = this.nep;
    let util = this.util;
    let args = this.args;
    let msg = this.msg;

    let validUrl = /^(?:https?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/watch\?v=([^&]+)/m; // RegExp for valid YouTube url
    let extractPlayListID = /[&|\?]list=([a-zA-Z0-9_-]+)/g;
    let isPlaylist =  /^.*(youtu.be\/|list=)([^#\&\?]*).*/;

    let id = extractPlayListID.exec(args.slice(1).join(' '));

    if (!args.slice(1)[0]) return util.embed(`:x: | You need to add either a **link, or a YouTube url**!`);
    else if (isPlaylist.exec(args.slice(1).join(' '))) { // Queue playlist
      curl.getJSON(`http://nepnep.tk/api/yt_playlist?id=${id[1]}&maxVideos=25&key=${nep.config.nepAPI.key}`, (err, resp, bod) => {
        if (err) return util.error(err);

        // Try catch ofc ofc
        try {

          // Delete authors message
          msg.delete({
            timeout: 3e3
          }).catch((err) => util.error(`Error when deleteing my message:\n\n${err}`));
          // Queue playlists videos
          let length = bod.result.videos.id.length > 25 ? 25 : bod.result.videos.info.url.length;
          for (let i = 0; i < length; i++) {
            curl.getJSON(`http://nepnep.tk/api/yt_video?maxResults=1&search=${bod.result.videos.info.url[i]}&key=${nep.config.nepAPI.key}`, (err, resp, body) => {
              if (err) return util.error(err);
              // Set author as msg.author
              body.videos.info.author = msg.author;
              // Push info into queue
              queue.push(body);
              });
            }
            // Send queue message
            msg.channel.send({
              embed: new nep.discord.MessageEmbed()
                .setDescription(`<:Selfie:390652489919365131> | Queued \`First 25 Videos of Playlist\` by **[${msg.author}]**`)
                .setColor(nep.rColor)
                .setFooter(`Use '${ nep.prefix}play' to play the queue!`, msg.author.displayAvatarURL())
            });
        } catch(err) {
          nep.util.error(err.stack);
          util.embed(`:x: | I tried but, nothing **could be found** for your Playlist URL!`);
        }

      });
    }
    else if (validUrl.exec(args.slice(1).join(' '))) { // If args is a link, queue the link
      // Search Nep API for this link
      curl.getJSON(`http://nepnep.tk/api/yt_video?maxResults=1&search=${ args.slice(1).join(' ')}&key=${nep.config.nepAPI.key}`, (err, resp, bod) => {

        // Try catch block ofc
        try {

          // Delete authors message
          msg.delete({
            timeout: 3e3
          }).catch((err) => util.error(`Error when deleteing my message:\n\n${err}`));
          // Set author as msg.author
          bod.videos.info.author = msg.author;
          // Push info into queue
          queue.push(bod);
          // Send queue message
          msg.channel.send({
            embed: new nep.discord.MessageEmbed()
              .setDescription(`<:Selfie:390652489919365131> | Queued \`${bod.videos.info.title[0]}\` by **[${bod.videos.info.author}]**`)
              .setColor(nep.rColor)
              .setFooter(`Use '${ nep.prefix}play' to play the queue!`, msg.author.displayAvatarURL())
          });

        } catch (err) {
          util.embed(`:x: | I tried but, nothing **could be found** for your URL!`);
        }

      });
    }
    else { // Search and list terms
      try {
        new Promise((resolve, reject) => {
          // Search Nep API
          if (args.slice(1).join(' ').replace(/[^\x20-\x7E]+/gu, ' ') == ' ') return util.embed(`:x: | Fully unicode titles **do not work**. Try something else or a link!`);

          curl.getJSON(`http://nepnep.tk/api/yt_video?maxResults=4&search=${args.slice(1).join(' ').replace(/[^\x00-\x7F]+/g, ' ')}&key=${ nep.config.nepAPI.key}`, (err, resp, bod) => {
            // return util.code('css', args.slice(1).join(' ').replace(/[^\x00-\x7F]+/g, ' ').replace(/\s+/g, ' '));
            if (err) return util.error(err);
            // Try catch ofc
            try {

              // Send list of search results

              util.embed(
                `**First 4 Results for** \`${ args.slice(1).join(' ')}\`\n\n` +
                `♥ [${bod.videos.info.title[0]}](${bod.videos.info.url[0]})\n` +
                `💙 [${bod.videos.info.title[1]}](${bod.videos.info.url[1]})\n` +
                `💚 [${bod.videos.info.title[2]}](${bod.videos.info.url[2]})\n` +
                `💜 [${bod.videos.info.title[3]}](${bod.videos.info.url[3]})\n` +
                `\n` +
                `💔 Cancel`
              ).then((m) => {
                // Reaction collector
                let collector = new nep.discord.ReactionCollector(m, m1 => m1.users.last().id == msg.author.id, {
                  time: 30e3,
                  dipose: true
                });
                // Heart array
                let hearts = [`♥`, `💙`, `💚`, `💜`, `💔`];

                // React hearts in order and catch error
                new Promise((resolve, reject) => {
                  m.react(hearts[0]).then(() => m.react(hearts[1])).then(() => m.react(hearts[2])).then(() => m.react(hearts[3])).then(() => m.react(hearts[4]));
                }).catch((err) => util.error(err));

                // When a reaction is hit, queue the video or cancel
                collector.on('collect', (mm) => {

                  switch (mm.emoji.name) {
                    case hearts[0]: // First choice
                      {
                        queueVideo(bod.videos.info.url[0]);
                        m.reactions.removeAll().catch((err) => err);
                        collector.stop();
                      }
                      break;
                    case hearts[1]: // Second choice
                      {
                        queueVideo(bod.videos.info.url[1]);
                        m.reactions.removeAll().catch((err) => err);
                        collector.stop();
                      }
                      break;
                    case hearts[2]: // Third choice
                      {
                        queueVideo(bod.videos.info.url[2]);
                        m.reactions.removeAll().catch((err) => err);
                        collector.stop();
                      }
                      break;
                    case hearts[3]: // Fourth choice
                      {
                        queueVideo(bod.videos.info.url[3]);
                        m.reactions.removeAll().catch((err) => err);
                        collector.stop();
                      }
                      break;
                    case hearts[4]: // Cancel
                      {
                        m.reactions.removeAll().catch((err) => util.embed(`I don't have permission to remove reactions!`, m));
                        collector.stop();
                      }
                      break;
                  }
                });

                function queueVideo(link) {
                  // Re-search API and queue info
                  curl.getJSON(`http://nepnep.tk/api/yt_video?maxResults=1&search=${link}&key=${nep.config.nepAPI.key}`, (err, respy, body) => {
                    body.videos.info.author = msg.author;
                    queue.push(body);
                    m.edit({
                      embed: new nep.discord.MessageEmbed()
                        .setDescription(`<:Selfie:390652489919365131> | Queued \`${body.videos.info.title[0]}\` by **[${body.videos.info.author}]**`)
                        .setColor(nep.rColor)
                        .setFooter(`Use '${ nep.prefix}play' to play the queue!`, msg.author.displayAvatarURL())
                    }).then(() => m.reactions.removeAll()).catch((err) => err);
                  });
                }

              }).catch((err) => util.error(err));

            } catch (err) {
              util.embed(`:x: | No **results could be found** for your query of \`${util.parseArgs(args.slice(1).join(' '))}\``);
            }

          });
        }).catch((err) => util.embed(`:x: | No **results could be found** for your query of \`${util.parseArgs(args.slice(1).join(' '))}\``));
      } catch (err) {
        util.embed(`:x: | No **results could be found** for your query of \`${util.parseArgs(args.slice(1).join(' '))}\``);
      }
    }

  }

  // ---------------------------------------------------------------------------

  shuffle(queue) { // Shuffle the queue
    if (!queue[0]) return this.util.embed(`:x: | The queue is **emptier than your bed**, I can't do that!`);

    shuffle(queue);
    return this.util.embed(`🔘 | Queue has been **shuffled** by **[${this.msg.author}]**!`);

    function shuffle(a) { // Shuffles an array
      for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
      }
      return a;
    }
  }

  // ---------------------------------------------------------------------------

  remove(queue) { // Remove items from the queue
    let int = parseInt(this.args.slice(1)); // Item to remove
    let util = this.util;

    if (!queue[0]) return this.util.embed(`:x: | There's **nothing to remove** since the queue is empty!`);
    else if (isNaN(int)) return this.util.embed(`:x: | Provide the **queue position** to remove!`);
    else if (int - 1 > queue.length) return this.util.embed(`:x: | Uhh yeah so \`${int}\` is **higher than the queue** itself, clam down!`);
    else if (queue[int - 1].videos.info.author !== this.msg.author && !this.msg.member.hasPermission(`ADMINISTRATOR`) && !this.checkRole()) return util.embed(`:x: | You can only remove items *you've* queued. Besids **admin perms people** of course!`);

    this.util.embed(`❎ | Removed \`${queue[int-1].videos.info.title[0]}\` by **[${this.msg.author}]**`);
    queue.remove(int - 1);


  }

  // ---------------------------------------------------------------------------

  clear(queue) { // Clears the queue
    let voiceConnection = this.msg.guild.voiceConnection;

    if (!this.checkRole() && !this.msg.member.hasPermission(`ADMINISTRATOR`)) return this.util.embed(`:x: | You do not have the role **'NeptuneDJ' or admin** permissions!`);
    else if (!queue[0]) return this.util.embed(`:x: | The queue is already empty!`);
    else if (voiceConnection) {
      this.util.embed(`⛔ | The queue **has been cleared** by **[${this.msg.author}]**, kbye!`);
      queue.splice(0, queue.length);
      voiceConnection.player.dispatcher.end();
      return this.msg.member.voiceChannel.leave();
    }

    this.util.embed(`⛔ | The queue **has been cleared** by **[${this.msg.author}]**, kbye!`);
    queue.splice(0, queue.length);
    return this.msg.member.voiceChannel.leave();


  }

  // ---------------------------------------------------------------------------

  checkRole() {
    let role = this.msg.guild.roles.find((r) => r.name.toLowerCase().startsWith('NeptuneDJ'.toLowerCase()));

    if (!role) return false;
    else if (!this.msg.member.roles.get(role.id)) return false;
    return true;
  }

  // ---------------------------------------------------------------------------


}

module.exports = QueueClass;
