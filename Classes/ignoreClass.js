class IgnoreClass {
  constructor(msg, util, args, nep, flag, suffix, postSuffix) {
    this.msg = msg;
    this.util = util
    this.args = args;
    this.nep = nep;
    this.flag = flag;
    this.suffix = suffix;
    this.postSuffix = postSuffix;
  }

  // -----------------------------------------------------------

  channel() {
    // Define class values as variables
    let msg = this.msg;
    let util = this.util;
    let args = this.args;
    let nep = this.nep;
    let flag = this.flag;
    let suffix = this.suffix;
    let postSuffix = this.postSuffix;

    // Check if suffix is present
    if (!suffix) return util.embed(`:x: | Provide a \`-allow\` to **allow use** of, \`-deny\` to **revoke use** of and \`-list\` to **list**.`);
    // Check if suffix is '-allow' or '-deny'
    else if (suffix.toLowerCase() !== '-allow' && suffix.toLowerCase() !== '-deny' && suffix.toLowerCase() !== '-list') return util.embed(`:x: | \`${util.parseArgs(suffix)}\` is invalid. Use \`-allow, -deny or -list\` instead!`);

    // Check whether to add or not
    switch (suffix.toLowerCase()) {
      case '-deny':
        // Get the channel
        util.listChannels(postSuffix.join(' ')).then((c) => {
          util.selectAll(`ignoring`, `guild`).then((row) => {
            let ingoringMsg = `🗒 | Now **ignoring commands** from \`${c.type} channel\` ${c} by **[${msg.author}]**`;
            let alreadyIgnoring = [];

            // Insert into table if guild has no ignore
            if (row.length <= 0) {
              return nep.connection.query(`INSERT INTO ignoring (guildId, channelId) VALUES (?, ?) ON DUPLICATE KEY UPDATE channelId = ${c.id}`, [msg.guild.id, c.id], function(err) {
                if (err) return util.error(err);
                // Send completed message
                util.embed(ingoringMsg);
              });
            }

            // Push all ignored channels into array
            for (let i = 0; i < row.length; i++) {
              alreadyIgnoring.push(row[i].channelId);
            }

            // Check if channel is already in ignore list
            if (alreadyIgnoring.indexOf(c.id) >= 0) return util.embed(`:x: | ${c} is **already** being ignored. \n\n Use \`${nep.prefix}ignore -channel -allow ${c.name}\` to allow.`);
            // If it's not, insert into database then send message
            nep.connection.query(`INSERT INTO ignoring (guildId, channelId) VALUES (?, ?) ON DUPLICATE KEY UPDATE channelId = ${c.id}`, [msg.guild.id, c.id], function(err) {
              if (err) return util.error(err);
              return util.embed(ingoringMsg);
            });

          }).catch((err) => util.error(err)); // Table Search
        }).catch((err) => util.error(err)); // Channel search
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-allow':
        util.listChannels(postSuffix.join(' ')).then((c) => {
          util.selectAll('ignoring', 'guild').then((row) => {
            let message = `❎ | Now **allowing commands** from \`${c.type} channel\` ${c} by **[${msg.author}]**!`;
            let message2 = `:x: | I was **never ignoring** \`${c.type} channel\` ${c}, *leaf* me alone! >:c`;
            let alreadyIgnoring = [];

            // If row is empty, nothing is being ignored
            if (row.length <= 0) return util.embed(message2);

            // Push all channels into array 
            for (let i = 0; i < row.length; i++) {
              alreadyIgnoring.push(msg.guild.channels.get(row[i].channelId));
            }

            // If channelId is in array send already ignoring
            if (alreadyIgnoring.indexOf(c) < 0) return util.embed(message2);
            // If not, unignore the channel and send message
            else return nep.connection.query(`DELETE FROM ignoring WHERE channelId = ${c.id} AND guildId = ${msg.guild.id}`, function(err) {
              if (err) return util.error(err);
              util.embed(message);
            });
          }).catch((err) => util.error(err)); // Table search
        }).catch((err) => util.error(err)); // Channel search
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-list':
        util.selectAll(`ignoring`, 'guild').then((row) => {
          let list = [];

          // Push channels into array
          for (let i = 0; i < row.length; i++) {
            if (row[i].channelId == null) { continue; }
            list.push(msg.guild.channels.get(row[i].channelId));
          }
          
          // If nothing in DB, send message
          if (row.length <= 0) return util.embed(`*Can't list anything if there's nothing to list <:WeSmart:421536576707887114>*`);

          // TODO: Add pages to this one day
          util.embed(`**Current Channels being Ignored**\n\n${list.map((ch) => ch.type == 'voice' ? `\`${ch.name} (Voice)\`` : ch.type == 'category' ? `\`${ch.name} (Category)\`` : ch).join(' | ')}`);

        }).catch((err) => util.error(err)); // Table search
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=
    }

  }

  // -----------------------------------------------------------

  user() {
    // Define class values as variables
    let msg = this.msg;
    let util = this.util;
    let args = this.args;
    let nep = this.nep;
    let flag = this.flag;
    let suffix = this.suffix;
    let postSuffix = this.postSuffix;

    // Check if suffix is present
    if (!suffix) return util.embed(`:x: | Provide a \`-allow\` to **allow use** of, \`-deny\` to **revoke use** of and \`-list\` to **list**.`);
    // Check if suffix is '-allow' or '-deny'
    else if (suffix.toLowerCase() !== '-allow' && suffix.toLowerCase() !== '-deny' && suffix.toLowerCase() !== '-list') return util.embed(`:x: | \`${util.parseArgs(suffix)}\` is invalid. Use \`-allow, -deny or -list\` instead!`);

    // Switch case for '-allow' and '-deny' 
    switch (suffix.toLowerCase()) {
      case '-allow':
        util.listUsers(postSuffix.join(' ')).then((u) => {
          util.selectAll('ignoring', 'guild').then((row) => {
            let message = `❎ | Now **allowing commands** from \`member\` ${u} by **[${msg.author}]**!`;
            let message2 = `:x: | I was **never ignoring** \`member\` ${u}, *leaf* me alone! >:c`;
            let alreadyIgnoring = [];

            // If row is empty, nothing is being ignored
            if (row.length <= 0) return util.embed(message2);

            // Push all users into array 
            for (let i = 0; i < row.length; i++) {
              alreadyIgnoring.push(row[i].userId);
            }

            // If userId is in array send already ignoring
            if (alreadyIgnoring.indexOf(u.id) < 0) return util.embed(message2);
            // If not, unignore the user and send message
            else return nep.connection.query(`DELETE FROM ignoring WHERE userId = ${u.id} AND guildId = ${msg.guild.id}`, function(err) {
              if (err) return util.error(err);
              util.embed(message);
            });
          }).catch((err) => util.error(err)); // Table search
        }).catch((err) => util.error(err)); // Channel search
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-deny':
        // Get the user
        util.listUsers(postSuffix.join(' ')).then((u) => {
          
          if (u.id == nep.user.id) return util.embed(`<:ChuckNorris:429692536412700673> | Not the best idea to make me ingore myself!`);
          
          util.selectAll(`ignoring`, `guild`).then((row) => {
            let ingoringMsg = `🗒 | Now **ignoring commands** from \`member\` ${u} by **[${msg.author}]**`;
            let alreadyIgnoring = [];

            // Insert into table if guild has no ignore
            if (row.length <= 0) {
              return nep.connection.query(`INSERT INTO ignoring (guildId, userId) VALUES (?, ?) ON DUPLICATE KEY UPDATE userId = ${u.id}`, [msg.guild.id, u.id], function(err) {
                if (err) return util.error(err);
                // Send completed message
                util.embed(ingoringMsg);
              });
            }

            // Push all ignored users into array
            for (let i = 0; i < row.length; i++) {
              alreadyIgnoring.push(row[i].userId);
            }

            // Check if channel is already in ignore list
            if (alreadyIgnoring.indexOf(u.id) >= 0) return util.embed(`:x: | ${u} is **already** being ignored. \n\n Use \`${nep.prefix}ignore -user -allow ${u.tag}\` to allow.`);
            // If it's not, insert into database then send message
            nep.connection.query(`INSERT INTO ignoring (guildId, userId) VALUES (?, ?) ON DUPLICATE KEY UPDATE userId = ${u.id}`, [msg.guild.id, u.id], function(err) {
              if (err) return util.error(err);
              return util.embed(ingoringMsg);
            });

          }).catch((err) => util.error(err)); // Table Search
        }).catch((err) => util.error(err)); // Channel search
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-list':
        util.selectAll(`ignoring`, 'guild').then((row) => {
          let list = [];

           // Push members into array
          for (let i = 0; i < row.length; i++) {
            if (row[i].userId == null) { continue; }
            list.push(`<@${row[i].userId}>`);
          }
          
          // If nothing in DB, send message
          if (!list[0]) return util.embed(`*Can't list anything if there's nothing to list <:WeSmart:421536576707887114>*`);

          // TODO: Add pages to this one day
          util.embed(`**Current Members being Ignored**\n\n${list.join(' | ')}`);

        }).catch((err) => util.error(err)); // Table search
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=
    }
  }

  // -----------------------------------------------------------

  role() {
    // Define class values as variables
    let msg = this.msg;
    let util = this.util;
    let args = this.args;
    let nep = this.nep;
    let flag = this.flag;
    let suffix = this.suffix;
    let postSuffix = this.postSuffix;

    // Check if suffix is present
    if (!suffix) return util.embed(`:x: | Provide a \`-allow\` to **allow use** of, \`-deny\` to **revoke use** of and \`-list\` to **list**.`);
    // Check if suffix is '-allow' or '-deny'
    else if (suffix.toLowerCase() !== '-allow' && suffix.toLowerCase() !== '-deny' && suffix.toLowerCase() !== '-list') return util.embed(`:x: | \`${util.parseArgs(suffix)}\` is invalid. Use \`-allow, -deny or -list\` instead!`);

    // Switch case for '-allow' and '-deny' 
    switch (suffix.toLowerCase()) {
      case '-allow':
        util.listRoles(postSuffix.join(' ')).then((r) => {
          util.selectAll('ignoring', 'guild').then((row) => {
            let message = `❎ | Now **allowing commands** from \`role\` ${r} by **[${msg.author}]**!`;
            let message2 = `:x: | I was **never ignoring** \`role\` ${r}, *leaf* me alone! >:c`;
            let alreadyIgnoring = [];

            // If row is empty, nothing is being ignored
            if (row.length <= 0) return util.embed(message2);

            // Push all roles into array 
            for (let i = 0; i < row.length; i++) {
              alreadyIgnoring.push(row[i].roleId);
            }

            // If roleId is in array send already ignoring
            if (alreadyIgnoring.indexOf(r.id) < 0) return util.embed(message2);
            // If not, unignore the role and send message
            else return nep.connection.query(`DELETE FROM ignoring WHERE roleId = ${r.id} AND guildId = ${msg.guild.id}`, function(err) {
              if (err) return util.error(err);
              util.embed(message);
            });
          }).catch((err) => util.error(err)); // Table search
        }).catch((err) => util.error(err)); // Channel search
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-deny':
        // Get the role
        util.listRoles(postSuffix.join(' ')).then((r) => {
          util.selectAll(`ignoring`, `guild`).then((row) => {
            let ingoringMsg = `🗒 | Now **ignoring commands** from \`role\` ${r} by **[${msg.author}]**`;
            let alreadyIgnoring = [];

            // Insert into table if guild has no ignore
            if (row.length <= 0) {
              return nep.connection.query(`INSERT INTO ignoring (guildId, roleId) VALUES (?, ?) ON DUPLICATE KEY UPDATE roleId = ${r.id}`, [msg.guild.id, r.id], function(err) {
                if (err) return util.error(err);
                // Send completed message
                util.embed(ingoringMsg);
              });
            }

            // Push all ignored roles into array
            for (let i = 0; i < row.length; i++) {
              alreadyIgnoring.push(row[i].roleId);
            }

            // Check if role is already in ignore list
            if (alreadyIgnoring.indexOf(r.id) >= 0) return util.embed(`:x: | ${r} is **already** being ignored. \n\n Use \`${nep.prefix}ignore -role -allow ${r.name}\` to allow.`);
            // If it's not, insert into database then send message
            nep.connection.query(`INSERT INTO ignoring (guildId, roleId) VALUES (?, ?) ON DUPLICATE KEY UPDATE roleId = ${r.id}`, [msg.guild.id, r.id], function(err) {
              if (err) return util.error(err);
              return util.embed(ingoringMsg);
            });

          }).catch((err) => util.error(err)); // Table Search
        }).catch((err) => util.error(err)); // Channel search
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=
      case '-list':
        util.selectAll(`ignoring`, 'guild').then((row) => {
          let list = [];

          // Push roles into array
          for (let i = 0; i < row.length; i++) {
            if (row[i].roleId === null) { continue; }
            list.push(`<@&${row[i].roleId}>`);
          }

          // If nothing in DB, send message
          if (!list[0]) return util.embed(`*Can't list anything if there's nothing to list <:WeSmart:421536576707887114>*`);
          
          // TODO: Add pages to this one day
          util.embed(`**Current Roles being Ignored**\n\n${list.join(' | ')}`);

        }).catch((err) => util.error(err)); // Table search
        break;
        // -=-=-=--=-=-=-=-=-=-=-=-=-=
    }
  }

  // -----------------------------------------------------------
  
  /*sendPage(type) { // Send pages for list
    // Define class values as variables
    let msg = this.msg;
    let util = this.util;
    let nep = this.nep;
    
    util.selectAll('ignoring', 'guild').then((row)=> {
      let list = [];
      let seperated = [];
      
      // Push items into array
      for (let i = 0; i < row.length; i++) {
        if (row[i][`${type}`] == null) { continue; }
        
        switch(type) {
          case 'channelId': 
            list.push(`<#${row[i][`${type}`]}>`);
          break;
          case 'userId': 
            list.push(`<@${row[i][`${type}`]}>`);
          break;
          case 'roleId': 
            list.push(`<@&${row[i][`${type}`]}>`);
          break;
        }       
      }
      
      // If more than 25, send pages
      if (list.length > 25) {
        while (list.length) {
          seperated.push(list.splice(0, 10).join('|'));
        }
      }
      
    }).catch((err) => util.error(err)); // Table Search
    
  }*/
  
  // -----------------------------------------------------------
  
}

module.exports = IgnoreClass;