const personalCat = `https://robohash.org/${new Date().valueOf()}?set=set4`;

class Cat {
    constructor(client, owner, row) {
        this.owner = owner; // User collection
        this.client = client; // Client
        this.row = row; // MySQL row
    }
    insertCat() { // INSERT data into cat table
        this.client.connection.query(`INSERT INTO cat (name, owner, url, rank) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE owner = ${this.owner.id}`, [
            'Unnamed', // Cat name
            this.owner.id, // Owner ID
            personalCat, // Cat URL Link
            this.rank // Rank of cat
        ]);
    }
    renameCat(name) { // 'Re'names cat in database with new name
        this.client.connection.query(`UPDATE cat SET name = \"${name}\" WHERE owner = ${this.owner.id}`);
    }
    newCat(url) { // Replaces old URL with new URL for cat
        this.client.connection.query(`UPDATE cat SET url = \"${personalCat}\" WHERE owner = ${this.owner.id}`);
    }
    updateCat(row, newValue, qutoes) {
        if (qutoes) return this.client.connection.query(`UPDATE cat SET ${row} = \"${newValue}\" WHERE owner = ${this.owner.id}`);
        else return this.client.connection.query(`UPDATE cat SET ${row} = ${newValue} WHERE owner = ${this.owner.id}`);
    }
    get rank() {
        return Math.floor(Math.random() * 100);
    }
}

module.exports = Cat;